/**
 * @property WeberTools.Tests.BaseTest WeberBaseTest
 */
ExampleTest = function(autoInitialize = true) {
    WeberTools.Object.extend(this, new WeberTools.Tests.BaseTest());

    this.name = 'BasicExampleTest';
    this.mainLoop = null;
    this.frequency = 500;
    this.state = true;
    this.loopInc = 0;

    this.process = function() {
        console.group('ExampleTestProcess');
        console.log("ExampleTest::process. It's called by BaseTest::mainProcess");

        var self = this;
        if(!this.isTestRunning()) { return false; }

        console.log("With the timersGestionary, we can store our timers (interval/timeout) clean & clean them easily.");
        console.log("this.timersGestionary.addTimer(callback, timer, type : interval|timeout);");
        this.timersGestionary.addTimer(function() {
            console.log("Hey, I am the spam Timer, kill me with the button !");
        }, 50, 'interval', 'spamTimer');

        this.mainLoop = setInterval(function() {
            if(!self.isTestRunning()) { return false; }
            if(self.loopInc ===  10) {
                return self.endTest(self.generateResult(true, 'Looped 10 times successfully', {data: 'some data here'}));
            }
            if(self.loopInc > 10) {
                return self.endTest(self.generateResult(false, 'We are at more than 10 loop, there is a problem', {increment: self.loopInc}));
            }
            console.log('mainLoop is running');
            self.loopInc++;
        }, this.frequency);

        console.groupEnd();
    };

    this.stopTheSpamPlease = function () {
        this.timersGestionary.clearTimerByName('spamTimer');
    };

    this.cleanTest = function () {
        this.loopInc = 0;
    };

    this.initialize = function (autoInitialize = true) {
        if(!autoInitialize) { return false; }
        //this.addChildTest('childTest', new WeberChildTest());
    };

    this.initialize(autoInitialize);
};