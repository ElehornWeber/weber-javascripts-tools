WalletManagerObject = function () {
    this.amount = 1;
    this.updateWallet = function (amount= 0) {
        // do stuff
        amount = parseFloat(amount);
        this.amount = WeberTools.Math.round(this.amount + amount, 2);
    };
};