HudWallet = function (template, key) {
  WeberTools.Object.extend(this, new WeberTools.Hud.HudElement(template, key));


  this.setTemplateEventsListeners = function () {
      var self = this;
      var walletButtons = this.instance.querySelectorAll('.btn-wallet-update');
      for(let i = 0; i < walletButtons.length; i++) {
          let walletButton = walletButtons[i];
          walletButton.addEventListener('click', function () { WalletManager.updateWallet(this.getAttribute('data-amount')); });
      }
  };

  this.importListeners = function () {
      this.refreshFrequency = 0;

      this.setTemplateEventsListeners();

      this.addListener('wallet-amount', function (HudElement, element) {
          HudElement.updateInstanceProperty(element, 'innerHTML', WalletManager.amount);
      });
  };
  this.importListeners();
};