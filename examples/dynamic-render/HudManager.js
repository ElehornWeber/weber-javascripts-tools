HudManager = function () {
    WeberTools.Object.extend(this, new WeberTools.Hud.BaseHudManager());

    this.loadTemplatesCollection = function () {
        this.addHudElement(new HudWallet(this.getTemplate('hud-wallet-template'), 'wallet.tpl'));
    };

    this.initializeConfiguration = function () {
        this.setTemplatePath('weber-javascripts-tools/assets/dynamic-render/templates');
        this.templates = {
            'wallet.tpl' : 'hud-wallet-template'
        };

        this.refreshMode = 'interval';
        this.refreshFrequency = 200;
    };

    this.initializeConfiguration();
};