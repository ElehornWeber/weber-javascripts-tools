ExampleModalManager = function() {
    WeberTools.Object.extend(this, new WeberTools.Modal.CoreModal());

    this.initializeModal = function () {
        this.setTemplatePath('weber-javascripts-tools/assets/modal');
        this.templates = {
            'modal.tpl' : 'hud-modal-template'
        };
        this.refreshMode = 'interval';
        this.refreshFrequency = 1000;

        this.addModal('basic', WeberTools.Modal.BasicModal, 'basic-modal-template');
        this.addModal('boolean', WeberTools.Modal.BooleanModal, 'boolean-modal-template');
    };
    this.initializeModal();
};
