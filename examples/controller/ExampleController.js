ExampleController = function () {
    WeberTools.Object.extend(this, new WeberTools.Controller.BaseController());
    WeberTools.Object.extend(this, new WeberTools.Timers.TimersGestionary());


    this.endProcess = function () {
        console.log('-----------');
        console.group('exampleController::endProcess');
            console.log("Every controllers have the responsability to clean their process before end, this is the aim of endProcess");
            console.log("Here, we need to kill timeouts and such things");
            console.log("----------------");
            console.log('Our Timer is managed by the TimersGestionary, so we only have to call this.clearTimers() to clear all of them !');
        console.groupEnd();
        this.clearTimers();

    };


    this.mainProcess = function () {
        console.log('exampleController::MainProcess !');
        var id = this.getParameter('id');

        this.addTimer(function() {
            console.log("I am a timeout called in mainProcess !")
        }, 200, 'timeout');

        if(!id) {
            console.log("ID not found, kill the mainProcess and start the endProcess");
            return this.endProcess();
        }

        console.log('this is my parameters :', this.parameters);
        console.log('it\'s a success; I got my id:',id, ' with the good type. I can work safely.');

        console.log("our controller job is finished, we can end the Process");
        this.endProcess();
    };


};