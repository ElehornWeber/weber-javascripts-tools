CoreAutoload = function() {
    this.loadingState =  false;
    this.nameSpacing = false;
    this.fileCache = true;
    this.basePath = window.location.origin;
    this.libraryBasePath = window.location.origin;
    this.scriptLocation = document.head;
    this.ProjectClasses = {};
    this.ClassesDependencies = {};
    this.loadedClasses = {};
    this.scriptLoaded = {};
    this.includeOrder = [];
    this.startTime = null;

    this.prepareToNewAutoloading = function () {
        this.loadingState = false;
        this.ProjectClasses = {};
        this.loadedClasses = {};
        this.scriptLoaded = {};
        this.includeOrder = [];
        this.startTime = null;
    };

    this.setBasePath = function(path) {
        if(!path) { this.basePath = window.location.origin; return; }
        this.basePath = window.location.origin+'/'+path;
    };
    this.setLibraryBasePath = function(path) {
        if(!path) { this.libraryBasePath = window.location.origin; return; }
        this.libraryBasePath = window.location.origin+'/'+path;
    };

    this.setScriptLocation = function(id) {
        if(!document.getElementById(id)) { return; }
        this.scriptLocation = document.getElementById(id);
    };

    this.addProjectClass = function( fileName, namespace = '', dependencies = {}, instantiate = false ) {
        if(!fileName) { return false; }

        var projectClass = {
            namespace : namespace,
            name: fileName,
            dependencies : dependencies,
            instantiate : instantiate
        };
        var filePath = this.getFilePath(projectClass);
        this.ProjectClasses[filePath] = projectClass;
        for(var dependency in dependencies) {
            if(!this.ClassesDependencies.hasOwnProperty(dependency)) { this.ClassesDependencies[dependency] = {}; }
            this.ClassesDependencies[dependency][filePath] = 1;
        }
    };

    this.defaultEndEvent = function() {
        this.loadingState = true;
        console.log('The whole process took ', Date.now() - this.startTime, 'ms');
    };

    this.endEvent = function() { };

    this.getProgression = function() {
        var amountLoaded = WeberTools.Object.getLength(this.scriptLoaded);
        var ratio = amountLoaded / WeberTools.Object.getLength(this.ProjectClasses);
        return Math.round(ratio * 100);
    };

    this.isFileLoaded = function (File) {
        if(typeof File === 'object' && this.loadedClasses[this.getFilePath(File)]) { return true; }
        else if(typeof File === 'string' && this.loadedClasses[File]) { return true; }
        return false;
    };

    this.getFilePath = function (File) {
        if(!File.namespace) { return File.name; }
        return File.namespace+"/"+File.name;
    };

    this.addLoadedFile = function(File) {
        this.loadedClasses[this.getFilePath(File)] = File;
    };

    this.dependenciesEvent = function(File) {
        var filePath = this.getFilePath(File);
        if(!this.ClassesDependencies.hasOwnProperty(filePath)) { return true; }
        for(let fileKey in this.ClassesDependencies[filePath]) {
            if(fileKey === filePath) { continue; }
            if(this.loadedClasses.hasOwnProperty(fileKey)) { continue; }
            if(!this.ProjectClasses.hasOwnProperty(fileKey)) { continue; }
            var fileToLoad = this.ProjectClasses[fileKey];
            this.loadFile(fileToLoad);
        }

        return true;
    };

    this.checkDependencies = function(File)
    {
        if(!File.hasOwnProperty('dependencies')) { return true; }
        if(WeberTools.Object.getLength(File.dependencies) === 0) { return true; }

        for(var dependency in File.dependencies) {
            if(!this.isFileLoaded(dependency)) { return false; }
        }
        return true;
    };

    this.insertScript = function(File, eventOnLoaded)  {
        if(this.isFileLoaded(File)) { return; }
        if(!this.checkDependencies(File)) { return; }

        var newScript = document.createElement("script");

        newScript.onload = newScript.onerror = function() {
            if(eventOnLoaded) { eventOnLoaded(); }
        };

        var scriptSrc = this.basePath+"/"+this.getFilePath(File)+".js";
        if(!this.fileCache) { scriptSrc += "?"+Date.now(); }
        newScript.src = scriptSrc;
        
        newScript.type = 'application/javascript';
        this.scriptLocation.appendChild(newScript);
    };

    this.loadFile = function (File) {
        var self = this;
        this.insertScript(File, function() {
            self.addLoadedFile(File);
            self.scriptLoaded[self.getFilePath(File)] = true;
            self.includeOrder.push(File);

            self.dependenciesEvent(File);
            if(self.nameSpacing) { WeberTools.loadClassIntoNamespace(File); }
            //console.log(File.name, 'Progression chargement : ', Math.round(self.getProgression())+'%');
            if(self.getProgression() === 100) { self.endEvent(); }
        });
    };

    this.load =  function (eventOnLoaded) {
        this.startTime = Date.now();
        if(eventOnLoaded) {
            this.endEvent = function() {
                if(this.loadingState) { return; }
                this.defaultEndEvent();
                eventOnLoaded();
            }
        }

        for(var fileKey in this.ProjectClasses) {
            var fileToLoad = this.ProjectClasses[fileKey];
            this.loadFile(fileToLoad);
        }
    };
    this.start = function (configFile, basePath, callback, nameSpacing = false) {
        var self = this;

        this.nameSpacing = nameSpacing;

        this.setBasePath(basePath);

        var fullPath = this.basePath;
        if(basePath !== '') { fullPath += '/'; }
        fullPath += configFile;

        WeberTools.Fetch.json(fullPath, function (data) {
            self.prepareToNewAutoloading();
            for(var dataIncr in data) {
                var element = data[dataIncr];
                self.addProjectClass(element.name, element.namespace, element.dependencies, element.instantiate);
            }

            self.load(function () { callback(); });
        });
    };

    delete CoreAutoload;
};


WeberTools = {
    CoreAutoload : new CoreAutoload(),
    LibrariesMapper : {},
    addLibraryToMap : function(classOriginName, newPath) {
        this.LibrariesMapper[classOriginName] = newPath;
    },
    isInLibraryMap : function (classOriginName) {
        if(this.LibrariesMapper[classOriginName]) { return true; }
        return false;
    },
    getClassFromLibraryMap : function (classOriginName) {
        if(!this.isInLibraryMap(classOriginName)) { return false; }
        return this.LibrariesMapper[classOriginName];
    },
    loadClassIntoNamespace : function(Class, endCallback = false) {
        var namespace = Class.namespace.replace('WeberToolsLibraries', '').replace('/', '');

        var newClass = WeberTools.Object.getClassByName(Class.name);
        if(Class.instantiate) {
            newClass = WeberTools.Object.instanciateFromName(Class.name);
        }

        var name = Class.name.replace('Weber', '');
        if (!WeberTools.hasOwnProperty(namespace)) { WeberTools[namespace] = {}; }
        if (namespace && namespace !== '') { WeberTools[namespace][name] = newClass; }
        else { WeberTools[name] = newClass; }


        WeberTools.addLibraryToMap(Class.name, newClass);
        WeberTools.Object.deleteClassByName(Class.name);

        if(endCallback && typeof  endCallback === 'function') { endCallback(); }
    },
    loadClassesIntoNamespaces : function(increment = 0, endCallback = false) {
        if(increment >= WeberTools.CoreAutoload.includeOrder.length) {
            if(typeof endCallback === 'function') {
                return endCallback();
            }
            return true;
        }
        var includedClass = WeberTools.CoreAutoload.includeOrder[increment];
        this.loadClassIntoNamespace(includedClass, function () {
            increment++;
            WeberTools.loadClassesIntoNamespaces(increment, endCallback);
        });
    },
    Autoload : function(configFile, config, callback = false, projectNamespacing = false) {
        var self = this;
        var libraryBasePath, basePath = '';

        if(config.hasOwnProperty('basePath') && config.basePath) { basePath = config.basePath; }
        if(config.hasOwnProperty('libraryBasePath') && config.libraryBasePath) {  libraryBasePath = config.libraryBasePath;}
        if(config.hasOwnProperty('scriptLocation') && config.scriptLocation) { self.CoreAutoload.setScriptLocation(config.scriptLocation); }
        if(config.hasOwnProperty('disableCache') && config.disableCache) {
            self.CoreAutoload.fileCache = false;
        }
        return self.CoreAutoload.start('WeberToolsConfig.json', libraryBasePath, function () {
            return self.CoreAutoload.start(configFile, basePath, callback, projectNamespacing);
        }, true);
    },
    Object : {
        getName : function(object) {
            if(object.hasOwnProperty('prototype')) { return object.prototype.constructor.name; }
            return object.__proto__.constructor.name;
        },
        updateHeritage : function(source, target) {
            var targetName = target;

            if(this.getName(target) && source.Heritages.hasOwnProperty(this.getName(target))) {
                targetName = this.getName(target);
            }
            if(!source.Heritages.hasOwnProperty(targetName)) {
                return source;
            }

            var heritage =  source.Heritages[targetName];
            for(var key in source) {
                if(key === 'Heritages') { continue; }
                var property = source[key];
                if(typeof property === 'function') { continue; }
                if(!heritage.hasOwnProperty(key)) { continue; }

                heritage[key] = source[key];
            }
            return source;
        },
        extend : function(source, target, keepHeritage = false, avoidTargetPointer = false, callback = false) {

            var targetObject = target;
            if(typeof target === 'function') {
                targetObject = new target();
            } else if(targetObject === null) {
                targetObject = {};
            } else if(avoidTargetPointer && typeof target === 'object') {
                targetObject = Object.assign({}, target, JSON.parse(JSON.stringify(target)));
            }
            
            if(typeof targetObject !== 'object') { return source; }

            if(keepHeritage) {
                if (!source.hasOwnProperty('Heritages')) { 
                    source.Heritages = {}; 
                    source.Heritages.callHeritage = function(heritageName, heritageCallback) {
                        if(!source.Heritages.hasOwnProperty(heritageName)) { return false; }
                        var heritage = source.Heritages[heritageName];
                        source = WeberTools.Object.updateHeritage(source, heritageName);

                        heritageCallback(heritage);
                    };
                }
                source.Heritages[this.getName(target)] = Object.assign({}, targetObject);
                delete targetObject.Heritages;
            }

            var finalObject = Object.assign(source, targetObject);

            if(callback && typeof callback === 'function') { callback(finalObject); }

            return finalObject;
        },
        instanciateFromName : function (objectName) {
            if(!this.classExists(objectName)) { return false; }

            var libraryClass = WeberTools.getClassFromLibraryMap(objectName);
            if(libraryClass) { return new libraryClass(); }

            return (Function('return new ' + objectName))();
        },
        classExists : function (objectName) {
            var libraryClass = WeberTools.getClassFromLibraryMap(objectName);
            if(libraryClass) { return true; }

            var type = (Function('return typeof ' + objectName))();
            if(type === 'function') { return true; }

            return false;
        },
        getClassByName : function (objectName) {
            var libraryClass = WeberTools.getClassFromLibraryMap(objectName);
            if(libraryClass) { return libraryClass; }
            return (Function('return ' + objectName))();
        },
        deleteClassByName : function (objectName) {
            if(!this.classExists(objectName)) { return false; }
            return (Function('delete ' + objectName))();
        },
        getProperties: function (object) { return Object.keys(object); },
        getLength: function (object) { return this.getProperties(object).length },
    },
    Fetch: {
        json: function(completePath, callback) {
            if(typeof fs !== 'undefined') {
                if(fs.existsSync(path.resolve('./..')+'/'+completePath)) { completePath = path.resolve('./..')+'/'+completePath; }
                if(!fs.existsSync(completePath)) { return false; }

                var file = fs.readFileSync(completePath);
                callback(JSON.parse(file));
                return;
            }
            if(typeof fetch !== 'undefined') {
                var result = fetch(completePath).then(function (response) {
                    if(!response.ok) {
                        console.error(response.status+' : '+response.statusText, completePath);
                        return;
                    }
                    var contentType = response.headers.get("content-type");
                    if(contentType && contentType.indexOf("application/json") !== -1) { return response.json(); }
                    return response.body;

                }).catch(function(error) {
                    console.log('Il y a eu un problème avec l\'opération fetch: ' + error.message);
                });

                return result.then(function (json) { callback(json); })
            }
        }
    },
    Math: {
        round: function (num, decimals) {
            if(decimals === 0) { return Math.round(num); }
            if(!decimals) { decimals = 2; }
            var base = Math.pow(10, decimals);
            return Math.round(num * base) / base;
        },

        randomInt: function (min, max = false) {return (min + Math.floor ((max - min + 1) * Math.random ())); },

        intInInterval : function(min, max, value, open = true) {
            if(value < min || value > max) { return false; }
            if(!open && (value === min || value === max)) { return false; }
            return true;
        },
        randomArrayKey : function(array, rangeAccepted = true) {
            if(rangeAccepted && array.length === 2) {
                return this.randomInt(array[0], array[1]);
            }
            var randomKey = this.randomInt(0, array.length-1);
            return array[randomKey];
        },
        average: function(numbers) {
            var total = 0;
            for(var i = 0; i < numbers.length; i++) { total += numbers[i]; }
            return total / numbers.length;
        },
        toPercentage: function (num) { return num*100; },
        toRatio: function (num) { return num/100; },
    },
    Hud : {
        FlashListenerCollection : {}
    }
};
