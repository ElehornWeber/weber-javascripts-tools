WeberBaseController = function () {
    this.route = '';
    this.parameters = {};
    this.variables = {};

    this.endProcess = function () {};
    this.getEndProcessData = function () {};
    this.mainProcess = function () {};

    this.getVariable = function (name) {
        if(!this.variables.hasOwnProperty(name)) { return false; }
        return this.variables[name];
    };
    this.clearVariable = function (name) {
        if(!this.getVariable(name)) { return false; }
        delete this.variables[name];

    };
    this.clearVariables = function () {
        for(var key in this.variables) { this.clearVariable(this.variables[key]); }
    };
    this.addVariable = function (name, element) {
        this.variables[name] = element;
    };

    this.updateParameters = function (parameters = {}) {
        if(!parameters) { return false; }
        this.parameters = parameters;
    };

    this.getParameter = function (key) {
        if(!this.parameters.hasOwnProperty(key)) { return false; }
        return this.parameters[key];
    };

    this.process = function (parameters = {}) {
        this.updateParameters(parameters);
        this.mainProcess();
    };

};