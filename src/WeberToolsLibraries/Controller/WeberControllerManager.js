WeberControllerManager = function () {
    this.Router = {};
    this.Controllers = {};

    this.currentController = null;

    this.routeExists = function (route) {
        if(!route) { return false; }
        if(!this.Router.hasOwnProperty(route)) { return false; }

        return true;
    };

    this.getRegexRoute = function (Route) {
        if(!Route) { return false; }

        var regexRoute = Route.path;
        for(var key in Route.parameters) {
            var parameter = Route.parameters[key];
            regexRoute = regexRoute.replace('$'+key, parameter);
        }
        return regexRoute;
    };

    this.buildPathUrl = function (path, parameters) {
        for(var key in parameters) { path = path.replace('$'+key, parameters[key]);}
        return path;
    };

    this.buildRouteUrl = function (Route, Parameters) {
        return this.buildPathUrl(Route.path, Parameters);
    };

    this.findRoute = function (path) {
        for(var i in this.Router) {
            var Route = this.Router[i];
            var regexRoute = this.getRegexRoute(Route);
            if(new RegExp(regexRoute).test(path)) { return Route; }
        }
        return false;
    };

    this.getRoute = function (route) {
        if(this.routeExists(route)) { return this.Router[route]; }
        var Route = this.findRoute(route);
        return Route;
    };


    this.getRouteByPath = function (path) {
        if(!this.Router.hasOwnProperty(path)) { return false; }
        return this.Router[path];
    };

    this.addController = function (controllerName, controllerObject) {
        this.Controllers[controllerName] = new controllerObject();
    };
    this.bindRouteToController = function (route, controllerName, parameters = {}, shortRoute = '') {
        this.Router[route] = {
            path: route,
            controllerName: controllerName,
            parameters : parameters
        };
        if(shortRoute && shortRoute !== route && route !== '') { this.Router[shortRoute] = this.Router[route]; }

        if(!this.Controllers[controllerName]) { return false; }

        this.Controllers[controllerName].route = route;
        this.Controllers[controllerName].parameters = parameters;
    };

    this.addRouteController = function (controllerName, route, controllerObject, parameters = {}, shortRoute = '') {
        if(!controllerName) { return false; }
        if(!route) { return false; }
        if(!controllerObject) { return false; }

        this.addController(controllerName, controllerObject);
        this.bindRouteToController(route, controllerName, parameters, shortRoute);
    };

    this.controllerExists = function (controllerName) {
        if(!controllerName) { return false; }
        if(!this.Controllers.hasOwnProperty(controllerName)) { return false; }
        if(typeof this.Controllers[controllerName].process !== 'function') { return false; }

        return true;
    };

    this.getController = function (controllerName) {
        if(!this.controllerExists(controllerName)) { return false; }
        return this.Controllers[controllerName];
    };

    this.getControllerNameByRoute = function (route) {
        var Route = this.getRoute(route);
        if(!Route) { return false; }
        return Route.controllerName;
    };
    this.getRouteByControllerName = function (key = '') {
        var controller = this.getController(key);
        if(!controller) { return false; }
        return this.getRoute(controller.route);
    };

    this.getControllerByRoute = function (route) {
        if(!this.Router.hasOwnProperty(route)) { return false; }
        return this.getController(this.getControllerNameByRoute(route));
    };

    this.cleanRouteParameters = function (Route, parameters) {
        for(var key in parameters) {
            if(!Route.parameters.hasOwnProperty(key)) { delete parameters[key]; continue; }
            if(!Route.path.match('$'+key)) { delete parameters[key]; continue; }

            var regex = new RegExp(Route.parameters[key]);
            if(!regex.test(parameters[key])) { delete parameters[key]; continue; }
        }

        return parameters;
    };

    this.extractParametersFromRoute = function (route, Route, parameters = {}) {
        var extractedParameters = {};
        var regexRoute = this.getRegexRoute(Route);
        var result = new RegExp(regexRoute).exec(route);
        if(!result) { return parameters; }

        var routeParameters = WeberTools.Object.getProperties(Route.parameters);
        for(var i = 0; i < routeParameters.length; i++) {
            if(!result[i+1]) { continue; }
            var parameterKey = routeParameters[i];
            extractedParameters[parameterKey] = result[i+1];
        }

        parameters = Object.assign(parameters, extractedParameters);
        return parameters;
    };

    this.controllerProcess = function (controllerName, parameters = {}, method = 'process') {
        var controller = this.getController(controllerName);
        if(!controller) { return false; }

        this.currentController = controller;

        if(method === "process") { return controller.process(parameters); }
        if(method === "endProcess") { return controller.endProcess(parameters); }
    };

    this.controllerProcessByRoute = function (route, parameters = {}, method = 'process') {
        var Route = this.getRoute(route);
        if(!Route) { return false; }
        parameters = this.extractParametersFromRoute(route, Route, parameters);
        parameters = this.cleanRouteParameters(Route, parameters);
        this.controllerProcess(Route.controllerName, parameters, method);
    };

    this.controllerEndProcessByRoute = function (route, parameters = {}) {
        this.controllerProcessByRoute(route, parameters, 'endProcess');
    };

    this.getCurrentControllerEndProcessData = function () {
        if(!this.currentController) { return false; }

        return this.currentController.getEndProcessData();
    };

    this.endCurrentController = function () {
        if(!this.currentController) { return false; }
        var data = this.getCurrentControllerEndProcessData();

        this.currentController.endProcess();
        this.currentController = null;

        return data;
    };


};