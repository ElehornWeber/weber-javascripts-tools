WeberAdvancedObject = function () {
    WeberTools.Object.extend(this, WeberTools.Object);
    this.getProperty = function (object, index) { return object[index]; };
    this.getLastProperty = function (object) { return this.getProperties(object)[this.getLength(object) - 1] };

    this.isArray = function (object) {
        if(typeof object != 'object') { return false; }
        if(object.length === null) { return false; }
        if(object.length === undefined) { return false; }
        if(object.length === false) { return false; }

        return true;
    };
    this.getRecursivePropertyValue = function (BaseObject, Path, separator = '/') {
        var result = this.findPropertyRecursive(BaseObject, Path, false, separator);
        if(!result) { return null; }
        if(!result.hasOwnProperty('object')) { return null;}
        return result.object[result.property];
    };

    this.findPropertyRecursive = function (BaseObject, Path, canCreate = false, separator = '/') {
        if(typeof Path === 'string') { Path = Path.split(separator); }
        var targetProperty = Path[0];
        if(Path.length > 1) {
            for(let i = 0; i < Path.length-1; i++) {
                var property = Path[i];
                if(!BaseObject[property]) {
                    if(!canCreate) { break; }
                    BaseObject[property] = {};
                }
                BaseObject = BaseObject[property];
            }
            targetProperty = Path[Path.length-1];
        }
        if(!targetProperty in BaseObject) { return null; }
        return {object : BaseObject, property : targetProperty };
    };

    this.updatePropertyByPath = function (BaseObject, Path, newProperty, targetParameterJoin = false, separator = '/') {
        if(targetParameterJoin) {
            if(typeof Path === 'object') { BaseObject[Path.join(separator)] = newProperty; return true;}
            BaseObject[Path] = newProperty;
            return true;
        }
        var result = this.findPropertyRecursive(BaseObject, Path, true);
        result.object[result.property] = newProperty;
    };

    this.objectToArray = function (object) {
        if(typeof object !== 'object') { return []; }
        if(this.isArray(object)) { return object; }
        var array = [];
        for(var key in object) { array[key] = object[key]; }
        return array;
    };

    this.instanceOf = function (source, target) {
        if(source instanceof target) { return true; }
        if(!source.hasOwnProperty('Heritages')) { return false; }

        for(var key in source.Heritages) {
            if(target === this.getClassByName(key)) { return true; }
            if(this.instanceOf(source.Heritages[key], target)) { return true; }
        }

        return false;
    };
    /**@param {Object} queryObject*/
    this.convertToQueryString = function (queryObject) {
        return new URLSearchParams(queryObject).toString();
    };
    this.convertQueryStringToObject = function (queryString = location.search) {
        return Object.fromEntries(new URLSearchParams(queryString));
    };

    //this duplicate will do a shallow copy: references can be keep
    this.duplicate = function (object) { return Object.assign({}, object) };
    //deepDuplicate will erase all references on the copy. But the stringify will erase all functions.
    this.deepDuplicate = function (object) { return JSON.parse(JSON.stringify(object)); };
    //fullDeepDuplicate, WIP, will in first, deepDuplicate to have every properties, then it will instantiate a new Object as an extend of the source.
    //fullDeepDuplicate = function (object, classModel) { this.extend(this.deepDuplicate(object), new window[classModel]())}
};