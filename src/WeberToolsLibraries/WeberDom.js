WeberDom = function () {
    this.Select = {
        findOptionBySelect : function(selectElement) {
            var value = selectElement.value;
            return selectElement.querySelector('option[value=' + value + ']');
        }
    };
    this.Check = {
        getValue : function (checkElement) {
            if(checkElement.checked) { return 1; }
            return 0;
        }
    }


    this.watchCollapse = function (sourceElement = document) {
        WeberTools.Dom.eventOnElements(function () {
            let groupName = this.getAttribute('wb-group-name');
            let collapseId = this.getAttribute('wb-collapse-id');

            WeberTools.Dom.foreachElements(function (element) {
                if(groupName !== element.getAttribute('wb-group-name')) { return; }

                if(collapseId === element.getAttribute('wb-collapse-id')) {
                    element.classList.remove('hidden');
                    return;
                }
                element.classList.add('hidden');
            }, '.wb-collapse');
        }, 'click', '.wb-collapse-btn', sourceElement);
    };


    this.foreachElements = function (event, query, sourceElement = document) {
        if(!event || typeof event !== "function") { return false; }

        var elements = sourceElement.querySelectorAll(query);
        if(!elements) { return false; }

        for(let i = 0; i < elements.length; i++) {
            let element = elements[i];
            event(element, i, elements);
        }
        return true;
    };

    this.eventOnElements = function (event, eventType = 'click', query, sourceElement) {
        if(!event || typeof event !== "function") { return false; }

        this.foreachElements(function (element) {
            element.addEventListener(eventType, event);
        }, query, sourceElement);
    };
};