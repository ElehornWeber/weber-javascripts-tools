/**
 * @property WeberTools.Timers.TimersGestionary WeberTimersGestionary
 */

WeberBaseTest = function() {

    this.name = '';
    this.state = true;
    this.result = {};
    this.mainLoop = null;
    this.timersGestionary = new WeberTools.Timers.TimersGestionary();
    this.childTests = {};
    this.childTestEnable = true;
    this.startTime = 0;
    this.endTime = 0;

    this.process = function () {};
    this.cleanTest = function () {};

    this.mainCleanTest = function () {
        this.state = true;
        this.result = {};

        if(this.mainLoop) { clearInterval(this.mainLoop); }
        this.timersGestionary.clearTimers();

        this.startTime = 0;
        this.endTime = 0;

        this.cleanTest();
    };

    this.mainProcess = function() {
        this.mainCleanTest();

        this.startTime = Date.now();
        this.process();

        //this.startChildrenTest();
    };

    this.getProcessTime = function () {
        return this.endTime - this.startTime;
    };

    this.getTestsResult = function() {
        console.group("Test Result of "+this.name);
            console.log(this.result);
        console.groupEnd();
        for(var testName in this.childTests) {
            var Test = this.childTests[testName];
            Test.getTestsResult();
        }
    };

    this.addChildTest = function (name, ChildTestObject) {
        ChildTestObject.name = name;
        this.childTests[name] = ChildTestObject;
    };

    this.startChildrenTest = function () {
        if(!this.childTestEnable) { return false; }
        for(var testName in this.childTests) {
            var Test = this.childTests[testName];
            Test.mainProcess();
        }
    };

    this.endTest = function(testResult, callback = false) {
        this.state = false;

        this.endTime = Date.now();
        testResult.time = this.getProcessTime();

        if(this.mainLoop) { clearInterval(this.mainLoop); }
        this.timersGestionary.clearTimers();

        this.result = testResult;

        this.startChildrenTest();

        if(callback && typeof callback === 'function') {
            callback();
        }

        this.getTestsResult();
    };

    this.generateResult = function(result, message = '', data = {}) {
        return {
            result : result,
            message : message,
            data : data,
            time: 0
        };
    };

    this.isTestRunning = function() {
        return this.state;
    };
    
    this.simulateClick = function(Element) {
        var event = new MouseEvent('click', {});
        Element.dispatchEvent(event);
    };

};