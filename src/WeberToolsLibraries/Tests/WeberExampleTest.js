/**
 * @property WeberTools.Tests.BaseTest WeberBaseTest
 */
WeberExampleTest = function(autoInitialize = true) {
    WeberTools.Object.extend(this, new WeberTools.Tests.BaseTest());

    this.name = 'BasicExampleTest';
    this.mainLoop = null;
    this.frequency = 500;
    this.state = true;
    this.loopInc = 0;

    this.process = function() {
        var self = this;
        if(!this.isTestRunning()) { return false; }

        this.timersGestionary.addTimer(function() {
            console.log('timer method called with addTimer')
        }, 400, 'interval');

        this.mainLoop = setInterval(function() {
            if(!self.isTestRunning()) { return false; }
            if(self.loopInc ===  3) {
                return self.endTest(self.generateResult(true, 'Looped 3 times successfully', {data: 'some data here'}));
            }
            if(self.loopInc > 3) {
                return self.endTest(self.generateResult(false, 'We are at more than 3 loop, there is a problem', {increment: self.loopInc}));
            }
            console.log('mainLoop is running');
            self.loopInc++;
        }, this.frequency);
    };

    this.initialize = function (autoInitialize = true) {
        if(!autoInitialize) { return false; }
        //this.addChildTest('childTest', new WeberChildTest());
    };

    this.initialize(autoInitialize);
};