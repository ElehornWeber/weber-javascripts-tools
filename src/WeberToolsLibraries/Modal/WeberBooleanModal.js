/**
 * @var WeberBasicModal WeberTools.Modal.BasicModal
 */
WeberBooleanModal = function (template, key) {
  WeberTools.Object.extend(this, new WeberTools.Modal.BasicModal(template, key));

    this.modalTitle = '';
    this.modalBody = '';
    this.modalFooter = null;

  this.yesButton = 'Oui';
  this.noButton = 'Non';

  this.yesEvent = function() { console.log('user said YES.'); };
  this.noEvent = function() { console.log('user said NO'); };

  this.setModalEvents = function() {
      let self = this;

      this.addListener('btn-choice.choice-no', function (HudElement, element) {
          HudElement.updateInstanceProperty(element, 'innerHTML', HudElement.noButton);
      });
      this.addListener('btn-choice.choice-yes', function (HudElement, element) {
          HudElement.updateInstanceProperty(element, 'innerHTML', HudElement.yesButton);
      });

      var choices = this.instance.querySelectorAll('.btn-choice');
      for(var i = 0; i < choices.length; i++) {
          var choice = choices[i];
          choice.addEventListener('click', function () {
              if(this.getAttribute('data-choice') === "1") { self.yesEvent(); }
              else { self.noEvent(); }
              self.remove();
          });
      }
  };

  this.importListeners = function () {
      this.refreshFrequency = 600;
      this.setBasicModalEvents();
      this.setModalEvents();
      this.setBasicModalListeners();
  };
};