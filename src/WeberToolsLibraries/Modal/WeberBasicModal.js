/**
 * @var WeberHudElement WeberTools.Hud.HudElement
 */
WeberBasicModal = function () {
    WeberTools.Object.extend(this, new WeberTools.Hud.HudElement());

    this.canBeClosed = true;

    this.modalTitle = '';
    this.modalBody = '';
    this.modalFooter = '';


    this.closeModalEvent = function () {
        if(!this.canBeClosed) { return false; }
        this.remove();
    };

    this.maskEvent = function () {
        if(!this.canBeClosed) { return false; }
        this.remove();
    };

    this.setBasicModalEvents = function () {
        var self = this;
        if(!this.instance) { return false; }

        var modalCloser = this.instance.querySelector('.modal-closer');
        if(modalCloser) {
            if(this.canBeClosed) {
                modalCloser.addEventListener('click', function () {
                    self.closeModalEvent();
                });
            } else { modalCloser.remove(); }
        }
    };

    this.toggleElement = function (element, condition) {
        if(!condition && !element.classList.contains('hidden')) { element.classList.add('hidden'); return; }
        if(condition && element.classList.contains('hidden')) { element.classList.remove('hidden'); }
    }

    this.setBasicModalListeners = function () {
        var self = this;
        this.addListener('modal-title .content', function (HudElement, element) {
            //self.toggleElement(element.parentElement, HudElement.modalTitle);
            HudElement.updateInstanceProperty(element, 'innerHTML', HudElement.modalTitle);
        });
        this.addListener('modal-body .content', function (HudElement, element) {
            //self.toggleElement(element.parentElement, HudElement.modalBody);
            HudElement.updateInstanceProperty(element, 'innerHTML', HudElement.modalBody);
        });
        this.addListener('modal-footer .content', function (HudElement, element) {
            //self.toggleElement(element.parentElement, HudElement.modalFooter);
            HudElement.updateInstanceProperty(element, 'innerHTML', HudElement.modalFooter);
        });
    };

    this.importListeners = function () {
        this.refreshFrequency = 250;
        this.setBasicModalEvents();
        this.setBasicModalListeners();
    };
};