/**
 * @property WeberTools.Hud.BaseHudManager WeberBaseHudManager
 */
WeberCoreModal = function() {
    WeberTools.Object.extend(this, new WeberTools.Hud.BaseHudManager());

    this.initializeModal = function () {};
    this.onCreateEvent = function () {
        document.body.classList.add('weber-modal-open');
    };

    /**@param {WeberHudElement} HudElement */
    this.onRemoveEvent = function (HudElement) {
        delete this.collection[HudElement.name];
        document.body.classList.remove('weber-modal-open');
    };

    this.modalCollection = {};

    this.hasModal = function() {
        if(WeberTools.Object.getLength(this.collection) > 0 ) { return true; }
        return false;
    };

    this.addModal = function(key, className, templateName) {
        this.modalCollection[key] = {class: className, template: templateName};
    };

    this.clearModal = function (modalKey) {
        if(!this.collection.hasOwnProperty(modalKey)) { return false; }
        this.collection[modalKey].remove();
    };

    this.clearAll = function () {
        for(var i in this.collection) {
            var modal = this.collection[i];
            modal.remove();
        }
    };

    this.getModalData = function(key) {
        if(!key || !this.modalCollection.hasOwnProperty(key)) { return false; }
        return this.modalCollection[key];
    };

    this.generateModalId = function() {
        return 'weber-modal-'+(WeberTools.Object.getLength(this.collection)+1);
    };

    this.generateModal = function(modalKey, data = {}) {
        var modalData = this.getModalData(modalKey);
        if(!modalData) { return false; }
        var Template = this.getTemplate(modalData.template);

        var modal = new modalData.class();
        modal.setTemplate(Template);
        modal.setName(this.generateModalId());
        modal.initializeInstance();

        this.onCreateEvent();

        WeberTools.Object.extend(modal, data);

        modal.importListeners();
        modal.refreshInstance(true);

        modal.hudManager = this;

        return this.addHudElement(modal);
    };

    this.initializeModal();
};
