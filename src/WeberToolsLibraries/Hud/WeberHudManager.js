/**
 * @property WeberTools.Hud.BaseHudManager WeberBaseHudManager
 */
WeberHudManager = function () {
    WeberTools.Object.extend(this, new WeberTools.Hud.BaseHudManager());

    /**
     * Create your own HudManagers with their HudElements to manage your dynamics elements !
     */
    this.loadTemplatesCollection = function () {
        //load your templates here
        //this.addHudElement(new BaseHud(this.getTemplate('template-id'), 'template.tpl'));
    };
};