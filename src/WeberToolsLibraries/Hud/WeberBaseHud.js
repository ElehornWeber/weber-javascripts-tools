/**
 * @property WeberTools.Hud.HudElement WeberHudElement
 */
WeberBaseHud = function (template, key) {
  WeberTools.Object.extend(this, new WeberTools.Hud.HudElement(template, key));


  this.setTemplateEventsListeners = function () {
      var self = this;

  };

  this.importListeners = function () {
      this.refreshFrequency = 0;

      this.setTemplateEventsListeners();
  };
  this.importListeners();
};