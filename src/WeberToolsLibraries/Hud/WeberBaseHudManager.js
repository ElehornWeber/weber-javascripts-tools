WeberBaseHudManager = function () {
    this.originUi = null;
    this.originHud = null;
    this.collection = {};

    this.loop = null;
    this.refreshMode = 'animationFrame';
    this.refreshFrequency = 0;

    this.internalBinding = function (eventKey, type) {};
    this.loadTemplatesCollection = function () {};

    this.onCreateEvent = function () { };

    /**@param {WeberHudElement} HudElement */
    this.onRemoveEvent = function (HudElement) {
        delete this.collection[HudElement.name];
    };

    this.templates = {};
    this.templatesPath = window.location.origin;
    this.setTemplatePath = function(path) {
        if(!path) { this.templatesPath = window.location.origin; return; }
        this.templatesPath = window.location.origin+'/'+path;
    };


    this.addHudElement = function (hudElement, originType = 'default-hud') {
        if(!hudElement.instance) { return false; }

        this.collection[hudElement.name] = hudElement;
        if(originType === 'default-hud' && typeof this.originHud === 'object') { this.originHud.appendChild(hudElement.instance); }
        else if(originType === 'default-ui' && typeof this.originUi === 'object') { this.originUi.appendChild(hudElement.instance); }
        else if(originType !== null && typeof originType === 'object') { originType.appendChild(hudElement.instance); }

        hudElement.refreshInstance();

        return hudElement;
    };

    this.stopProcess = function () {
        if(this.refreshMode === 'animationFrame') { cancelAnimationFrame(this.loop); }
        else if(this.refreshMode === 'interval') { clearInterval(this.loop); }
    };

    this.removeChild = function(childName) {
        if(!this.collection.hasOwnProperty(childName)) { return false; }

        this.collection[childName].remove();

        this.collection[childName] = null;
        delete this.collection[childName];
    };

    this.removeChildren = function() {
        for(var childName in this.collection) { this.removeChild(childName); }
    };

    this.refreshAll = function () {
        var self = this;
        for(var i in this.collection) {
            var hudElement = this.collection[i];
            hudElement.refreshInstance();
        }

        if(this.refreshMode === 'animationFrame') {
            this.loop = window.requestAnimationFrame(function () { self.refreshAll(); });
        }
    };


    this.getTemplate = function (templateName) {
        if(!this.templates.hasOwnProperty(templateName)) { return  false;}
        return this.templates[templateName];
    };
    this.loadTemplates = function (loadedCallback) {
        var self = this;
        var loadedCount = 0;
        var total = WeberTools.Object.getLength(this.templates);
        if(total === 0) {
            loadedCallback();
            return;
        }
        for(let templateName in this.templates) {
            let template = this.templates[templateName];
            var targetTemplate = templateName;
            if(parseInt(templateName) == templateName && typeof parseInt(templateName) === 'number') {
                targetTemplate = template;
            }

            var targetUrl = this.templatesPath+'/'+targetTemplate+'.html';
            if(targetTemplate.slice(0,2) === '//') {
                targetUrl = window.location.origin+'/'+targetTemplate.slice(2)
            }

            var request = new XMLHttpRequest();
            request.open('GET', targetUrl,true);
            request.responseType = "document";
            request.send();
            request.onload = function() {
                if(!this.response.body.querySelector('section')) {
                    var template = document.createElement('template');
                    template.innerHTML = this.response.body.innerHTML;
                    //template.setAttribute('id', templateName);
                    self.templates[template.getAttribute('id')] = template;
                    return;
                }

                var responseTemplates = this.response.body.querySelector('section').querySelectorAll('template');
                for (var i = 0; i < responseTemplates.length; i++) {
                    var currentTemplate = responseTemplates[i];

                    if(typeof template === 'object') {
                        if(!template.includes(currentTemplate.getAttribute('id'))) { continue; }
                    }
                    if(typeof template === 'number') { /*1 : take all, (by default) */ }

                    self.templates[currentTemplate.getAttribute('id')] = currentTemplate;
                }

                loadedCount++;
                if(loadedCount >= total) { loadedCallback(); }
            };

            //delete self.templates[templateName];
        }
    };

    this.setOriginUi = function (originUi) {
        if(!originUi) { return false; }
        this.originUi = document.getElementById(originUi);
    };
    this.setOriginHud = function (originHud) {
        if(!originHud) { return false; }
        this.originHud = document.getElementById(originHud);
    };

    this.setRenderLocation = function (originUi, originHud) {
        this.setOriginUi(originUi);
        this.setOriginHud(originHud);
    };

    this.startLoop = function () {
        if(this.loop) { clearInterval(this.loop); }

        var self = this;
        if(this.refreshMode === 'animationFrame') {
            this.loop = window.requestAnimationFrame(function() { self.refreshAll(); });
        } else if(this.refreshMode === 'interval') {
            this.loop = setInterval(function () { self.refreshAll(); }, this.refreshFrequency);
        }
    };

    this.initialize = function () {

        var self = this;
        this.loadTemplates(function () {
            self.loadTemplatesCollection();
        });

        this.startLoop();
    };
};