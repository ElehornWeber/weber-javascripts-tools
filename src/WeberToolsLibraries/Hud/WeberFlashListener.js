WeberFlashListener = function(object, property, event, parameters = {}) {
    this.id = null;

    this.attachedObject = object;
    this.objectProperty = property;
    this.event = event;
    this.parameters = parameters;

    this.Bank = null;
    this.state = 1;
    this.Timer = null;

    this.updateCount = 0;
    this.preventUpdateCount = 0;
    this.lastUpdateTimestamp = 0;

    this.onEndEvent = function () {};
    this.getDefaultParameters = function () {
        return {
            id : this.generateFlashListenerId(),
            timer : 500,
            currentTimer : 500,
            decelaration : {
                frequency : 5,
                rate : 0.1,
                max : 6
            },
            timerType : 'interval',
            checkType : 'equal',
            checkStop : null,
            onEndEvent : null,
            updateLimit : 0,
        };
    };
    this.getDecelaration = function () {
        var inactivityIncrement = this.preventUpdateCount % this.parameters.decelaration.frequency;
        inactivityIncrement = Math.min(this.parameters.decelaration.max, inactivityIncrement);
        var additionalTime = this.parameters.timer * (1 + this.parameters.decelaration.rate * inactivityIncrement);
        return Math.round(additionalTime);
    };

    this.getAttachedObjectProperty = function () {
        var result = WeberTools.AdvancedObject.findPropertyRecursive(this.attachedObject, this.objectProperty, false, '.');
        if(!result) { return null; }

        return result.object[result.property];
    };

    this.updateBank = function () {
        if(!this.attachedObject) { return false; }

        var property = this.getAttachedObjectProperty();
        if(property === null) { return false; }

        if(property && typeof property === 'object') {
            this.Bank = WeberTools.Object.extend(property, {}, false, true);
            return;
        }
        this.Bank = property;
    };

    this.end = function () {
        this.Timer.clearTimers();

        this.onEndEvent();
    };

    this.checkUpdateLimit = function () {
        if(this.parameters.updateLimit < 1) { return true; }
        if(this.parameters.updateLimit > this.updateCount) { return true; }

        this.end();
        return false;
    };

    /**
     * @return {boolean}
     */
    this.checkUpdateEqual = function () {
        var property = this.getAttachedObjectProperty();
        if(property === null) {
            if(this.Bank) { return true; }
            return false;
        }
        if(this.Bank === property) { return false; }
        return true;
    };

    /**
     * @return {boolean}
     */
    this.checkUpdate = function () {
        if(typeof this.parameters.checkType === 'function') {
            return this.parameters.checkType(this.getAttachedObjectProperty(), this.Bank);
        }
        if(this.parameters.checkType === 'equal') {
            return this.checkUpdateEqual();
        }
        return true;
    };

    this.generateFlashListenerId = function () {
        return 'weber-flash-listener-'+(WeberTools.Object.getLength(WeberTools.Hud.FlashListenerCollection)+1);
    };

    this.addFlashListenerToCollection = function () {
        if(WeberTools.Hud.FlashListenerCollection[this.id]) {
            WeberTools.Hud.FlashListenerCollection[this.id].end();
            delete WeberTools.Hud.FlashListenerCollection[this.id];
        }
        WeberTools.Hud.FlashListenerCollection[this.id] = this;

    };

    this.initialize = function () {
        var self = this;

        this.parameters = WeberTools.Object.extend(this.getDefaultParameters(), this.parameters);
        this.parameters.currentTimer = this.parameters.timer;

        this.id = this.parameters.id;
        if(typeof this.parameters.onEndEvent === 'function') {
            this.onEndEvent = this.parameters.onEndEvent;
        }

        this.addFlashListenerToCollection();

        this.updateBank();

        this.event(this.Bank, this.parameters);

        this.Timer = new WeberTools.Timers.TimersGestionary();

        var eventCallable = function() {
            if(self.preventUpdateCount > 0) {
                var deceleration = self.getDecelaration();
                if (deceleration !== self.parameters.timer) {
                    self.Timer.addTimer(eventCallable, deceleration, self.parameters.timerType, 'main');
                }
            }
            if(!self.state) {  self.preventUpdateCount++; return false; }
            if(!self.checkUpdate()) { self.preventUpdateCount++; return false; }
            self.updateBank();

            self.event(self.Bank, self.parameters);
            self.updateCount++;
            self.preventUpdateCount = 0;
            self.lastUpdateTimestamp = Date.now();

            self.checkUpdateLimit();
        };

        this.Timer.addTimer(eventCallable, this.parameters.timer, this.parameters.timerType, 'main');

    };

    this.initialize();
};