WeberHudElement = function() {
    this.listeners = {};

    this.childName = null;
    this.parent = null;
    this.hudManager = null;
    this.children = [];

    this.name = '';
    this.template = null;
    this.instance = null;

    this.visible = true;
    this.importedListeners = false;

    // 0 = Each refreshInstance, other : MS delay
    this.refreshFrequency = 0;
    this.lastRefresh = 0;

    this.timers = new WeberTools.Timers.TimersGestionary();

    this.instanceInitializedEvent = function() { };
    this.removeHudEvent = function() {};

    this.setTemplate = function (Template) {
        if(!Template) { return false; }

        if(typeof Template === 'string') {
            var parser = new DOMParser();
            var content = parser.parseFromString(Template, 'text/html');
            Template = content.body.children[0];
        }
        this.template = Template;
    };

    this.setName = function (name = '') {
        this.name = name.replace('-template', '');
    };

    this.initializeInstance = function (importedTemplate = true) {
        if(!this.template) { return false; }

        if(importedTemplate) {
            this.createInstanceFromImportedTemplate();
            this.refreshInstance(1);
            return true;
        }

        this.instance = this.template;
        if(!this.importedListeners) {
            this.instanceInitializedEvent();
            this.importedListeners = true;
        }
        this.refreshInstance(1);
        return true;
    };

    this.contentToElement = function (content, hudClass = false) {
        if(!hudClass) { hudClass = WeberTools.Hud.HudElement; }

        var hudElement = new hudClass();
        hudElement.setTemplate(content);
        hudElement.initializeInstance(false);
        return hudElement;
    };

    /**
     * @param {String} targetProperty
     * @param {String} url
     * @param {Object} parameters
     * @param {WeberHudElement | boolean} generatedHudElement
     */
    this.importContentFromUrl = function (targetProperty, url, parameters = {}, generatedHudElement = false) {
        var self = this;
        var result = WeberTools.AdvancedObject.findPropertyRecursive(this, targetProperty, true, '.');

        var requestCallback = function (response) {
            var importedContent = self.importContent(response);
            if(!generatedHudElement) { result.object[result.property] = importedContent; }
            else { result.object[result.property] = self.contentToElement(importedContent, generatedHudElement); }
            if(result.property === 'instance') {
                self.setTemplate(importedContent);
                self.initializeInstance(false);
            }
        };

        if(axios !== undefined) {
            axios.get(url, { params: parameters }).then(function (response) { requestCallback(response.data); });
            return true;
        }

        var request = new XMLHttpRequest();
        request.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) { requestCallback(this.response); }
        };
        request.open("GET", url+"?"+WeberTools.AdvancedObject.convertToQueryString(parameters), true);
        request.send();
    };

    this.importContent = function (content, deep = 1)
    {
        if(content instanceof HTMLElement) { return content.cloneNode(deep); }

        content = new DOMParser().parseFromString(content, "text/html");
        var finalContent = document.createElement('section');
        finalContent.append(...content.body.childNodes);
        return finalContent;
    }

    this.createInstanceFromImportedTemplate = function () {
        if(!this.template) { return false; }
        if(!this.template.content) { return false; }

        var fullTemplate = document.importNode(this.template.content, 1);
        if(!fullTemplate) { return false; }

        var Content = fullTemplate.querySelector('section');
        Content.setAttribute('id', this.name);
        this.instance = Content;

        if(!this.importedListeners) {
            this.instanceInitializedEvent();
            this.importedListeners = true;
        }
    };

    this.hide = function () {
        this.visible = false;
        this.instance.classList.add('hidden');
    };
    this.show = function () {
        this.visible = true;
        this.instance.classList.remove('hidden');
    };

    this.toggle = function () {
        if(this.visible) { this.hide(); return; }
        this.show();
    };

    this.addChild = function (name, ChildElement, appendLocation = false) {
        ChildElement.parent = this;
        ChildElement.childName = name;
        this.children[name] = ChildElement;

        if(appendLocation) {
            var locationElement = false;
            if(appendLocation instanceof HTMLElement) { locationElement = appendLocation; }
            else if(typeof appendLocation === 'string') { locationElement = this.instance.querySelector(appendLocation); }

            if(locationElement && ChildElement.instance) { locationElement.appendChild(ChildElement.instance); }
        }

        ChildElement.refreshInstance();
        return this.children[name];
    };
    this.getChild = function (name) { return this.children[name]; };

    this.removeChildren = function() {
        if(!this.children) { return false; }
        for(var childName in this.children) {
            this.removeChild(childName)
        }
    };

    this.removeChild = function (name) {
        if(!this.children.hasOwnProperty(name)) { return false; }
        
        delete this.children[name].parent;

        this.children[name].remove();
        this.children[name] = null;

        delete this.children[name];
    };

    this.remove = function () {
        this.timers.clearTimers();

        this.removeHudEvent();
        this.removeListeners();
        this.removeChildren();

        if(!this.parent) {
            if(this.instance && this.instance.parentElement) {
                this.instance.parentElement.removeChild(this.instance);
            }
            this.instance = null;
        }  else {
            this.parent.removeChild(this.childName);
        }

        if(this.hudManager) { this.hudManager.onRemoveEvent(this); }
        
        if(this.instance && this.instance.parentElement) { this.instance.parentElement.removeChild(this.instance); }

        this.instance = null;
    };

    this.addListener = function (templateClass, method, global = false) {
        if(global) { templateClass = 'global-listener'; }
        if(this.listeners.hasOwnProperty(templateClass)) { return false; }

        this.listeners[templateClass] = { method: method, global: global}
    };
    this.removeListener = function (templateClass) { this.listeners[templateClass] = null; delete this.listeners[templateClass]; };

    this.removeListeners = function() {
        for(var key in this.listeners) {
            this.removeListener(key);
        }
    };

    /**
     * @param {HTMLElement} instanceElement
     * @param {Function|'default'} testMethod
     * @param {Function|'default'} assignMethod
     */
    this.updateInstanceProperty = function(instanceElement, propertyName, newProperty, testMethod = 'default', assignMethod = 'default') {
        var futureProperty = newProperty;
        if(typeof newProperty === 'function') { futureProperty = newProperty(); }
        if(newProperty instanceof WeberTools.Hud.HudElement) { futureProperty = newProperty.instance; }
        if(!instanceElement) { return; }

        //TEST PARTS
        if(testMethod === 'default' && this.lastRefresh !== 0) {
            if(futureProperty instanceof HTMLElement) {
                if(instanceElement.isEqualNode(futureProperty) || instanceElement.isSameNode(futureProperty)) { return false; }
                if(instanceElement.firstElementChild) {
                    var firstChild = instanceElement.firstElementChild;
                    if(firstChild.isEqualNode(futureProperty) || firstChild.isSameNode(futureProperty)) { return false; }
                }
            } else if(instanceElement[propertyName] ===  futureProperty) { return false; }
        } else if(typeof testMethod === 'function' && !testMethod(instanceElement, futureProperty)) { return false; }

        //ASSIGN PARTS
        if(assignMethod === 'default') {
            if(propertyName  === 'innerHTML' || propertyName  === 'innerText' || propertyName  === 'outerHTML' || propertyName  === 'text') {
                if(futureProperty instanceof HTMLElement) {
                    if(!instanceElement.firstChild) { instanceElement.appendChild(document.createElement('div')); }
                    instanceElement.replaceChild(futureProperty, instanceElement.firstChild);
                }
                else { instanceElement[propertyName] = futureProperty; }
            }

            return true;
        }

        assignMethod(instanceElement, futureProperty);
        return true;
    };


    this.refreshChildren = function () {
        if(!this.children) { return false; }

        for(let childKey in this.children) {
            var child = this.children[childKey];
            if(!child) {
                delete this.children[childKey];
                continue;
            }
            child.refreshInstance();
        }
    };

    this.refreshInstance = function (manual = false) {
        if(!this.instance) { return false; }
        if(!this.visible) { return false; }
        if(this.refreshFrequency === -1 && !manual) { return false;}
        
        var now = Date.now();
        if(!manual && this.refreshFrequency > 0 && now - this.lastRefresh < this.refreshFrequency) { return; }
        this.refreshChildren();
        
        for(var templateClass in this.listeners) {
            var listener = this.listeners[templateClass];
            var element = this.instance;
            if(!listener.global) {
                element = this.instance.querySelector('.' + templateClass);
                if (!element) { continue; }
            }
            var result = listener.method(this, element);
        }

        this.lastRefresh = now;
    };


    this.makeDraggable = function (Element, dragPreview = false) {
        Element.addEventListener('dragstart', function (Event) {
            Event.dataTransfer.effectAllowed = 'move';
            if(dragPreview) { }
            else { Event.dataTransfer.setDragImage(new Image(), 0, 0); }
            this.setAttribute('dragstartx', Event.layerX);
            this.setAttribute('dragstarty', Event.layerY);

            Element.style.zIndex = 1;
        });
        Element.addEventListener('drag', function (Event) {
            var screenOffset = {x : 0, y : 0};
            //Event.dataTransfer.dropEffect = 'none';
            var position = {
                x : Event.clientX - parseInt(this.getAttribute('dragstartx')) - screenOffset.x,
                y : Event.clientY - parseInt(this.getAttribute('dragstarty')) - screenOffset.y,
            };
            Element.style.left = position.x+'px';
            Element.style.top = position.y+'px';
        });
        Element.addEventListener('dragend', function (Event) {
            var screenOffset = {x : 0, y : 0};
            var position = {
                x : Event.clientX - parseInt(this.getAttribute('dragstartx')) - screenOffset.x,
                y : Event.clientY - parseInt(this.getAttribute('dragstarty')) - screenOffset.y,
            };
            Element.style.left = position.x+'px';
            Element.style.top = position.y+'px';
        });
    };
};