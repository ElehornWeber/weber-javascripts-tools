WeberPerformanceTest = function (autostart = true) {
    this.start = null;
    this.end = null;
    if(autostart) { this.start = Date.now(); }

    this.startTest = function () {
        this.start = Date.now();
        this.end = null;
    };

    this.endTest = function () { this.end = Date.now(); };

    this.result = function (autoEnd = false) {
        if(autoEnd) { this.endTest(); }

        if(!this.end) { return  false; }
        return this.end - this.start;
    };
};