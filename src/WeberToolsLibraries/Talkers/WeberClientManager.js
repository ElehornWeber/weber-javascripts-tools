/**
 * @property WeberTools.Talkers.CoreClientManager WeberCoreClientManager
 */
WeberClientManager = function () {
    WeberTools.Object.extend(this, new WeberTools.Talkers.CoreClientManager());

    this.registerClientListener = function () {
        console.info('Start to register clients');

        this.addClientListener(new ExampleListener(true));
        console.info('listeners are loaded');

    };
};