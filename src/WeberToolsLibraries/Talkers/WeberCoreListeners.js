WeberCoreListeners = function() {
    this.key = '';
    this.messageId = 'id';
    this.mode = 'key';

    /** @type {WebSocket} this.Socket */
    this.Socket = null;

    this.url = '';
    this.port = '';
    this.isSecure = false;

    this.Library = {};
    this.nameToKeyMapper = {};

    this.subscribedTalkerEventMessages = {};

    this.processingState = true;

    //0 to disable automatic reconnection
    this.reconnectionTimer = 5000;
    this.reconnectionTimeout = null;

    this.connectionSuccessEvent = function () {};
    this.postProcessOnClose = function (event) {};
    this.postProcessMessage = function (event, callback) {};
    this.postProcessSendMessage = function (event) {};

    this.onMessageEvent = function (event, callback = false) {
        var data = event;
        if(event instanceof MessageEvent) { data = JSON.parse(event.data); }
        else if(typeof event === 'string') { data = JSON.parse(event); }

        if(typeof data !== 'object') { data = { message : data }; }

        /**@var {WeberTalkerMessage} talkerMessage  */
        var talkerMessage = this.getMessageByName(data[this.messageId]);
        if(!talkerMessage) { talkerMessage = this.getMessage(data[this.messageId]); }
        if(!talkerMessage) { return false; }

        this.bindSubscribedEventMessages(talkerMessage);

        talkerMessage.onMessage(data);
        this.postProcessMessage(data, callback);
    };

    /**@param {WeberTalkerMessage} talkerMessage */
    this.bindSubscribedEventMessages = function (talkerMessage) {
        if(!talkerMessage) { return false; }
        if(this.subscribedTalkerEventMessages.hasOwnProperty(talkerMessage.key)) {
            for(var key in this.subscribedTalkerEventMessages[talkerMessage.key]) {
                var method = this.subscribedTalkerEventMessages[talkerMessage.key][key];
                if(key === 'pre') { talkerMessage.preMethodEvent = method; }
                else if(key === 'onmessage') { talkerMessage.onMessage = method; }
                else if(key === 'post') { talkerMessage.postMethodEvent = method; }
            }
        }
    };
    /**
     * @param key
     * @param method
     * @param type : pre, post, onmessage
     */
    this.subscribeTalkerEventMessage = function (key, method, type = 'post') {
        if(!this.subscribedTalkerEventMessages.hasOwnProperty(key)) { this.subscribedTalkerEventMessages[key] = {}; }
        this.subscribedTalkerEventMessages[key][type] = method;

        var talkerMessage = this.getMessage(key);
        if(!talkerMessage) { return true;}
        this.bindSubscribedEventMessages(talkerMessage);
    };

    this.registerAllListeners = function() {};
    this.registerBaseListeners = function() {};

    this.addMessage = function (key, keyName, method) {
        var newMessage = new WeberTools.Talkers.TalkerMessage(key, keyName, method);
        this.Library[newMessage.key] = newMessage;
        this.nameToKeyMapper[newMessage.keyName] = key;
    };

    this.getMessage = function(key) {
        if(!this.Library[key]) { return false; }
        return this.Library[key];
    };

    this.getKeyByName = function (name) {
        if(!this.nameToKeyMapper[name]) { return false; }
        return this.nameToKeyMapper[name];
    };
    this.getMessageByName = function(name) {
        return this.getMessage(this.getKeyByName(name));
    };

    this.getConnexionUrl = function () {
        if(!this.port) { return this.url; }
        return this.url+':'+this.port;
    };

    this.isSocketInstance = function () {
        if(!this.Socket) { return false; }
        if(!this.Socket instanceof WebSocket) { return false; }

        return true;
    };

    this.updateReconnectionTimer = function(timer, forceReconnection = false) {
        this.reconnectionTimer = timer;
        if(!forceReconnection && (this.isSocketInstance() && this.Socket.readyState !== WeberTools.Map.SocketStateMap.getValue('CLOSED'))) {
            return false;
        }

        if(this.reconnectionTimeout) { clearTimeout(this.reconnectionTimeout); }
        if(forceReconnection) {
            this.prepareConnection(true);
            return true;
        }
        this.startReconnectionProcess(true);
    };

    this.startReconnectionProcess = function (autoStart, callback) {
        var self = this;
        this.reconnectionProcess(function () {
            self.prepareConnection(autoStart, callback)
        });
    };

    this.reconnectionProcess = function (eventOnReconnection) {
        if(!this.reconnectionTimer) { return false; }
        this.reconnectionTimeout = setTimeout(eventOnReconnection, this.reconnectionTimer);
    };

    this.prepareConnection = function(autoStart = true, callback = null) {
        var self = this;
        if(this.isSocketInstance()) {
            if(this.Socket.readyState === this.Socket.CONNECTING) {
                return;
            }
            this.disconnect();
        }
        try {
            var connectionSecurity = 'ws';
            if(this.isSecure) { connectionSecurity = 'wss'; }
            self.Socket = new WebSocket(connectionSecurity + '://' + self.getConnexionUrl());
            if (autoStart) { self.connect(callback); return; }
            if (callback && typeof callback === 'function') { callback(); }
        } catch(exception) {
            //console.log('Exception on connexion : ', exception);
            self.startReconnectionProcess(autoStart, callback);
            return false;
        }
    };
    this.connect = function (callback = false) {
        var self = this;

        if(!this.isSocketInstance()) { return false; }

        this.Socket.addEventListener('error', function(event) {
            console.info("Connexion error.", event);
            self.postProcessOnClose(event);
            self.startReconnectionProcess(true, callback);
        });
        this.Socket.addEventListener('open', function () {
            console.info("Connexion etablished at "+self.url, ":",self.port);

            self.Socket.addEventListener('close', function(event) {
                console.info("Connexion closed.", event);
                self.postProcessOnClose(event);

                self.startReconnectionProcess(true, callback);
            });

            self.Socket.addEventListener('message', function(event) {
                self.onMessageEvent(event);
            });
            
            if(callback) { callback(); }
            if(self.connectionSuccessEvent && typeof self.connectionSuccessEvent === 'function') {
                self.connectionSuccessEvent();
            }
        });
    };

    this.send = function (id, data = {}, callback = false) {
        data[this.messageId] = id;
        if(!this.isSocketInstance() || this.Socket.readyState !== 1) { return false; }
        this.Socket.send(JSON.stringify(data));

        this.postProcessSendMessage(data);
    
        if(!callback || typeof callback !== 'function') { return true; }
        var self = this;

        var tempEvent =  function(event) {
            var eventData = JSON.parse(event.data);
            if(callback && typeof callback === 'function') { 
                var result = callback(eventData[self.messageId]); 
                if(result) { self.Socket.removeEventListener('message', tempEvent, false); }
            }
        };
        this.Socket.addEventListener('message', tempEvent, false);
    };

    this.disconnect = function () {
        if(!this.isSocketInstance()) { return; }
        this.Socket.close();
    };

    this.initialize = function (autoStart = true, callback = null) {
        var self = this;
        this.prepareConnection(autoStart, function () {
            self.registerBaseListeners();
            self.registerAllListeners();
            if(callback) { callback(); }
        });

    };

};