/**
 * @property WeberTools.Talkers.CoreListeners WeberCoreListeners
 */
WeberExampleListener = function (autoStart = false) {
    WeberTools.Object.extend(this, new WeberTools.Talkers.CoreListeners());

    this.key = 'example';
    this.url = '127.0.0.1:8080';
    this.mode = 'keyName';

    /** LISTENERS  */
    this.exampleMethodWS = function(data) {
        console.log('this is the method called when you receive a socket key 1 or at message-key-name ')
    };

    /** SENDERS  */


    this.registerBaseListeners = function () {
        var self = this;

        this.addMessage(1, 'message-key-name', function(data) {
            self.exampleMethodWS(data);
        });
    };

    this.initialize(autoStart);
};