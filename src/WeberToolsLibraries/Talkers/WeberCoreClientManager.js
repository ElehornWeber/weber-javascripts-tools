WeberCoreClientManager = function () {
    this.Listeners = {};

    this.initialize = function () {

    };

    this.registerClientListener = function () {

    };

    this.addClientListener = function (Listener) {
        if(this.Listeners.hasOwnProperty(Listener.key)) {
            console.error('The Listener ', Listener.key, 'already exists');
            return false;
        }
        this.Listeners[Listener.key] = Listener;
        return true;
    };

    this.getClientListener = function (clientId) {
        if(!this.Listeners.hasOwnProperty(clientId)) { return false; }
        return this.Listeners[clientId];
    };

    this.connectClient = function (clientId) {
        var Listener = this.getClientListener(clientId);
        if(!Listener) { return false; }
        return Listener.connect();
    };

    this.disconnectClient = function (clientId) {
        var Listener = this.getClientListener(clientId);
        if(!Listener) { return false; }
        return Listener.disconnect();
    };

    this.connectAllClients = function () {
        for(var key in this.Listeners) {
            var Listener = this.getClientListener(key);
            this.connectClient(key);
        }
    };

    this.disconnectAllClients = function () {
        for(var key in this.Listeners) {
            var Listener = this.getClientListener(key);
            this.disconnectClient(key);
        }
    };
};