WeberTalkerMessage = function(key, keyName, method) {
    this.key = key;
    this.keyName = keyName;
    this.calledAmount = 0;

    this.preMethodEvent = function (data, next) {
        if(typeof next === 'function') { next(data); }
    };
    this.postMethodEvent = function (data) {};

    this.method = method;

    this.onMessage = function (data) {
        var self = this;
        this.calledAmount++;
        this.preMethodEvent(data , self.method(data, function () {
            self.postMethodEvent(data);
        }));
    };
};