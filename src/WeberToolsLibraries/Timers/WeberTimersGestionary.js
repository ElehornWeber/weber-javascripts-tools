WeberTimersGestionary = function () {
    this.timers = {};
    this.timersNameMap = {};

    this.clearTimer = function (timerId) {
        if(!timerId) { return false; }

        clearTimeout(timerId);
        clearInterval(timerId);
    };

    this.clearTimers = function () {
        for(let intervalId in this.timers) { this.clearTimer(intervalId); }
    };

    this.addTimerName = function (name, timerId) {
        if(!name || !timerId) { return false; }

        this.timersNameMap[name] = timerId;
    };

    this.getTimerByName = function (name) {
        if(!this.timersNameMap.hasOwnProperty(name)) { return false; }
        return this.timersNameMap[name];
    };

    this.clearTimerByName = function (name) {
        this.clearTimer(this.getTimerByName(name));
    };

    this.addTimer = function (callable, timer, type = 'interval', name = false) {
        if(timer <= 0) { return false; }

        if(name) { this.clearTimerByName(name); }

        var intervalId = null;
        if(type === 'timeout') {
            intervalId = setTimeout(callable, timer);
        } else if(type === "interval") {
            intervalId = setInterval(callable, timer);
        }
        if(intervalId === null) { return false; }

        this.timers[intervalId] = callable;

        if(name) {
            this.addTimerName(name, intervalId);
        }
    };
};