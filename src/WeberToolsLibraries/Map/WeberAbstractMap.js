WeberAbstractMap = function () {
    this.map = {};
    this.reversedMap = {};

    this.getValue = function (key) {
        if(!this.map.hasOwnProperty(key)) { return false; }
        return this.map[key];
    };

    this.getKey = function (value) {
        if(!this.reversedMap.hasOwnProperty(value)) { return false; }
        return this.reversedMap[value];
    };

    this.generateReversedMap = function () {
        this.reversedMap = {};
        for(var key in this.map) {
            var value = this.map[key];
            this.reversedMap[value] = key
        }
    };

    this.addToMap = function (key = '', value = '') {
        if(key === '') { return false; }
        this.map[key] = value;
        this.generateReversedMap();
    };

    this.addObjectToMap = function (object = {}) {
        if(typeof object !== 'object') { return false; }

        for(var key in object) {
            var value = object[key];
            this.map[key] = value;
        }
        this.generateReversedMap();
    };

};