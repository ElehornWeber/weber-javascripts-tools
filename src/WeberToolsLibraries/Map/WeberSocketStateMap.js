WeberSocketStateMap = function() {
    WeberTools.Object.extend(this, new WeberTools.Map.AbstractMap(), true);

    this.initialize = function () {
        this.addToMap('CONNECTING', WebSocket.CONNECTING);
        this.addToMap('OPEN', WebSocket.OPEN);
        this.addToMap('CLOSING', WebSocket.CLOSING);
        this.addToMap('CLOSED', WebSocket.CLOSED);
    };
    this.initialize();
};