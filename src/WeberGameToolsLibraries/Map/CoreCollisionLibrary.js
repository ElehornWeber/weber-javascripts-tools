CoreCollisionLibrary = function () {

    this.actualCollisionData = [];

    this.testLayerPolygonialCollision = function (Actor, movement) {
        var actorPolygon = Actor.Character.getActorPolygon(false, movement);
        var response = new SAT.Response();
        for(var colIndex = 0; colIndex <  Game.MapManager.collisionsPolygons.length; colIndex++) {
            response.clear();
            var result = SAT.testPolygonPolygon(
                actorPolygon,
                Game.MapManager.SATCollisionsPolygons[colIndex],
                response
            );
            if(!result) { continue; }
            return response;
        }
        return false;
    };

    this.testActorPolygonialCollisions = function(Actor, movement) {
        return this.testLayerPolygonialCollision(Actor, movement);

        var fullDistance = Math.abs(movement.x) + Math.abs(movement.y);
        var steps = 1 + Math.round(fullDistance/8);
        var stepDistance = new Vector(movement.x/steps, movement.y/steps);

        for(var stepInc  = 0; stepInc <= steps; stepInc++) {
            var movementProgress = new Vector();
            movementProgress.x = stepDistance.x * (stepInc+1);
            movementProgress.y = stepDistance.y * (stepInc+1);
            var collisionResult = this.testLayerPolygonialCollision(Actor, movementProgress);
            if(!collisionResult || collisionResult.length === 0) { continue; }

            return collisionResult;
            //return movementProgress;
        }
        return false;
       // return movement;
    };

    this.updateGeometryCollisionCache = function (Player) {
        //test on y+1 & y-1
        var bottomCollision = this.testActorPolygonialCollisions(Player, new Vector(0,1));
        if(bottomCollision) {
            //console.log('bottom collision');
            // console.log('is on the ground');
            Player.Character.GeometryCache.axisCollision.y = -1;
            return;
        }
        //console.log('no bottom collision');

        var topCollision = this.testActorPolygonialCollisions(Player, new Vector(0,-1));
        if(topCollision) {
            //console.log('head stick to a wall');
            Player.Character.GeometryCache.axisCollision.y = 1;
            return;
        }

       // console.log('falling');
        Player.Character.GeometryCache.axisCollision.y = 0;
        return;
    };


    this.fullPlayerCollisionMovementProcess = function (Player, movement) {
        var result = this.testActorPolygonialCollisions(Player, movement);
        if(!result) { return new Vector(); }

        //var actorPolygon = Player.Character.getActorPolygon(false, movement);

        var movementReduction = new Vector(
            result.overlapV.x,
            result.overlapV.y
        );

        movementReduction.x = Math.round(movementReduction.x);
        movementReduction.y = Math.round(movementReduction.y);

        return movementReduction;
    };
};