WeberMapManager = function () {
    WeberTools.Object.extend(this, new WeberTools.GameLibrary.Map.MapBuilderLibrary());

    // 0 : not ready, 1: ready to be used
    this.status = 0;

    this.key = '';
    this.name = '';
    this.description = '';

    this.collisionLayersRefs = [];
    this.events = {};
    this.actors = {};

    this.Cell = new WeberTools.GameLibrary.Geometry.Size(16,16);
    this.Size = new WeberTools.GameLibrary.Geometry.Size(640,640);

    this.Ratio = {
        coefficient : 0.37,
        metric: 'm',
        meterToPx : function(meters) { return Math.floor(meters * this.coefficient); },
        pxToMeter  : function (pixels) { return Math.floor(pixels / this.coefficient); }
    };

    this.layers = {};
    this.getLayer = function (mapLayer) {
        if(!this.layers[mapLayer.type].hasOwnProperty(mapLayer.name)) { return false; }
        return this.layers[mapLayer.type][name];
    };

    this.addLayer = function (mapLayer) {
        if(!this.layers.hasOwnProperty(mapLayer.type)) { this.layers[mapLayer.type] = {}; }
        this.layers[mapLayer.type][name] = mapLayer;
    };


    this.loadMap = function (configFile, configCallback = false) {
        var self = this;
        WeberTools.Fetch.json(configFile, configCallback);
    };


    this.cellToPx = function (cells, axe = 'width') { return Math.floor(cells * this.Cell[axe]); };
    this.pxToCell = function (px, axe = 'width') { return Math.round(px / this.Cell[axe]); };

    this.convertPositionToCell = function (Position, cellScale =  1, nearest = false) {
        if(typeof Position === 'number') {
            var result = Position / (this.Cell.width * cellScale);
            if(nearest) { result = Math.round(result); } else { result = Math.floor(result); }
            return result;
        }
        var result =  new Vector(
            Position.x / (this.Cell.width * cellScale),
            Position.y / (this.Cell.height * cellScale)
        );
        if(nearest) { result.x = Math.round(result.x); result.y = Math.round(result.y); }
        else { result.x = Math.floor(result.x); result.y = Math.floor(result.y); }
        return result;
    };
    this.convertCellToPosition = function (Cell, cellScale = 1, nearest) {
        if(typeof Cell === 'number') {
            var result = Cell * (this.Cell.width * cellScale);
            if(nearest) { result = Math.round(result); } else { result = Math.floor(result); }
            return result;
        }
        var result =  new Vector(
            Cell.x * (this.Cell.width * cellScale),
            Cell.y * (this.Cell.height * cellScale)
        );
        if(nearest) { result.x = Math.round(result.x); result.y = Math.round(result.y); }
        else { result.x = Math.floor(result.x); result.y = Math.floor(result.y); }
        return result;
    };

    this.getCellPosition = function (Position, cellScale = 1, nearest = false) {
        return this.convertCellToPosition(
            this.convertPositionToCell(Position, cellScale, nearest),
            cellScale,
            nearest
        );
    };

    this.getCellsBetweenVector = function (startingVector,  endVector) {
        var startingCell = this.getCellPosition(startingVector);
        var endCell = this.getCellPosition(endVector);
        var Cells = {};
        Cells[startingCell.getKey()] = startingCell;
        Cells[endCell.getKey()] = endCell;
        if(WeberTools.GameLibrary.Geometry.VectorLibrary.isSameVector(startingCell, endCell)) { return Cells; }

        var delta = new WeberTools.GameLibrary.Geometry.Vector(
            this.pxToCell(endCell.x - startingCell.x, 'width'),
            this.pxToCell(endCell.y - startingCell.y, 'height')
        );

        for(var y = 0; y <= delta.y; y++) {
            var CurrentCell = new WeberTools.GameLibrary.Geometry.Vector(startingCell.x, startingCell.y);
            CurrentCell.y = startingCell.y + this.Cell.height*y;
            for(var x = 0; x <= delta.x; x++) {
                CurrentCell.x = startingCell.x + x*this.Cell.width;
                Cells[CurrentCell.getKey()] = new WeberTools.GameLibrary.Geometry.Vector(CurrentCell.x, CurrentCell.y);
            }
        }

        return Cells;
    };
};