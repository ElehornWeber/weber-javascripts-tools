WeberMapImageLayer = function () {
    this.id = 0;
    this.name = "";
    this.source = null;
    this.imagePath = "";
    this.opacity = 1;
    this.type = "imagelayer";
    this.visible = true;
    this.Position = new WeberTools.GameLibrary.Geometry.Vector();
    this.Size = new WeberTools.GameLibrary.Geometry.Size();
    this.loaded = 0;

    this.getRectangle = function () {
        return new WeberTools.GameLibrary.Geometry.Rectangle(
            this.Position,
            this.Size
        );
    };

    this.hide = function () {
        this.opacity = 0;
        this.visible = false;
    };
    this.show = function (opacity = 1) {
        this.opacity = opacity;
        this.visible = true;
    };

    this.loadImage = function (imagePath, callback = false) {
        this.imagePath = imagePath;
        if (typeof window !== 'undefined' && window) {
            var self = this;
            this.source = new Image();
            this.source.src = this.imagePath;
            this.source.addEventListener('load', function () {
                self.Size = new WeberTools.GameLibrary.Geometry.Size(this.width, this.height);
                self.loaded = true;
                if(callback) { callback(self); }
            });
        }
    }
};