WeberMapBuilderLibrary = function () {


    this.buildFullMap = function (mapName, callback = false) {
        var performancesTest = new WeberTools.Performance.PerformanceTest(true);

        var mapLayer = new WeberTools.GameLibrary.Map.MapImageLayer();
        mapLayer.name = mapName;
        mapLayer.show(1);
        mapLayer.loadImage(mapName, function () {
            console.log('Map full building took ', performancesTest.result(true), 'ms');
            if(callback) { callback(mapLayer); }
        });
        this.addLayer(mapLayer);

        return mapLayer;
    };
};