WeberPointer = function() {
    this.pointerStyle = 'default';
    this.Position = new Vector();

    this.getCursorCells =  function () {
        return new Vector(this.Position.x, this.Position.y);
    };

    this.getPosition = function () {
        return this.Position;
    };
};