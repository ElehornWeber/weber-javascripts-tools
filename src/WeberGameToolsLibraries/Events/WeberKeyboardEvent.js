WeberKeyboardEvent = function (keyboardEvent) {
    this.key = null;
    this.keyCode = null;
    this.type = 0;
    this.time = null;

    this.initialize = function (keyboardEvent) {
        this.key = keyboardEvent.key;
        this.keyCode = keyboardEvent.keyCode;
        this.type = 0;
        if(keyboardEvent.type === 'keydown') { this.type = 1; }

        this.time = Date.now();
    };
    this.initialize(keyboardEvent);
};