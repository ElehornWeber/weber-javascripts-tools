WeberControlAction = function (key, state = 1) {
    this.key = key;
    this.startTime = Date.now();
    this.time = Date.now();
    this.endTime = false;
    this.state = 0;
    this.used = 0;
    this.new = true;

    this.startEvent = function () {};
    this.endEvent = function () {};

    this.updateUsed = function () {
        this.used++;
        this.new = false;
    };

    this.updateTime = function () {
        this.time = Date.now();
    };
    this.updateStartTime = function () {
        this.time = Date.now();
        this.startTime = Date.now();
    };

    this.updateState = function (newState) {
        if(this.state === newState) { return false;  }
        if(newState) {
            this.time = Date.now();
            this.startTime = Date.now();
            this.new = true;
            this.used = 0;
            this.startEvent();
        } else {
            this.endTime = Date.now();
            this.endEvent();
        }

        this.state = newState;
    };

    this.updateState(state);
};