/**
* @namespace Geometry
*/

WeberCell = function (x, y, width, height, scale) {
    this.Position = new Vector(x, y, scale);
    this.size  = new Size(width, height,  scale)
};