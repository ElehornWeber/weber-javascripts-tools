WeberVectorLibrary = function () {
    this.generateVectorFromKey = function(vectorKey, separator = '/') {
        var vectorPositions = vectorKey.split(separator);
        return new Vector(vectorPositions[0], vectorPositions[1]);
    };

    this.generateVectorKey = function (vector, useScale = false, separator = '/') {
        var key = vector.x + separator + vector.y;

        if (useScale) { key += separator + scale; }
        return key;
    };

    this.checkIfVectorExist = function (array, vector) {
        if(array.hasOwnProperty(this.generateVectorKey(vector))) { return true; }
        return false;
    };

    this.isSameVector = function (firstVector, secondVector) {
        if(firstVector.x  !== secondVector.x) { return false; }
        if(firstVector.y  !== secondVector.y) { return false; }
        return true;
    };

};