/**
 * @namespace Geometry
 */

WeberSize = function (width = 0, height = 0, scale = 'px') {
    this.width = width;
    this.height = height;
    this.scale = scale;
};