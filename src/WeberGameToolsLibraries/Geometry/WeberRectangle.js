WeberRectangle = function (Origin, Size) {
    this.Origin = Origin;
    this.Size = Size;
    this.Points = [];

    this.duplicate = function (PositionRatio = false, SizeRatio = false) {
        var Origin = new WeberTools.GameLibrary.Geometry.Vector(this.Origin.x, this.Origin.y);
        var Size = new WeberTools.GameLibrary.Geometry.Size(this.Size.width, this.Size.height);
        if(PositionRatio) {
            Origin.x *= PositionRatio.width;
            Origin.y *= PositionRatio.height;
        }
        if(SizeRatio) {
            Size.width *= SizeRatio.width;
            Size.height *= SizeRatio.height;
        }

        return new WeberTools.GameLibrary.Geometry.Rectangle(Origin, Size);
    };

    this.getPoint = function (axeX = 'left', axeY = 'top') {
        var point = Object.assign({}, this.Origin);
        if(axeX === 'right') { point.x += this.Size.width; }
        if(axeY === 'bottom') { point.y += this.Size.height; }
        return point;
    };
    this.updatePoints = function () {
        this.Points = [
            this.getPoint('left', 'top'),
            this.getPoint('right', 'top'),
            this.getPoint('right', 'bottom'),
            this.getPoint('left', 'bottom'),
        ];
    };

    this.setSize = function (width = 0, height = 0) {
        this.Size = new WeberTools.GameLibrary.Geometry.Size(width, height);
        this.updatePoints();
    };
    this.setOrigin = function (x = 0, y= 0) {
        this.Origin = new WeberTools.GameLibrary.Geometry.Vector(x, y);
        this.updatePoints();
    };


    this.isPointIn = function (point) {
        var Intrasect = { x: false, y: false };

        var rectangleOrigin = this.getPoint('left', 'top');
        var rectangleLastPoint = this.getPoint('right', 'bottom');

        if(point.x >= rectangleOrigin.x && point.x <= rectangleLastPoint.x) { Intrasect.x = true; }
        if(point.y >= rectangleOrigin.y && point.y <= rectangleLastPoint.y) { Intrasect.y = true; }

        if(Intrasect.x && Intrasect.y) { return true; }
        return false;
    };

    this.containsRectangle = function(rectangleTarget) {
        for(var i = 0; i < rectangleTarget.Points.length; i++) {
            var point = rectangleTarget.Points[i];
            var result = this.isPointIn(point);
            if(!result) { return false; }
        }
        return true;
    };

    this.intersectRectangle = function(rectangleTarget) {
        if(
            this.Points[0].x <= rectangleTarget.Points[1].x &&
            this.Points[1].x >= rectangleTarget.Points[0].x &&

            this.Points[1].y <= rectangleTarget.Points[2].y &&
            this.Points[2].y >= rectangleTarget.Points[1].y
        ) {
            return true;
        }

        return false;
    };

    this.updatePoints();
};