/**
 * @namespace Geometry
 */

WeberVector = function (x = 0, y = 0, scale = 'px') {
    this.x = x;
    this.y = y;
    this.scale = scale;

    this.getKey = function () {
        return WeberTools.GameLibrary.Geometry.VectorLibrary.generateVectorKey(this);
    };
};