WeberCoreCameraScaling = function() {
    this.Scale = {
        Default : new WeberTools.GameLibrary.Geometry.Size(16, 16, 'px'),
        Current : new WeberTools.GameLibrary.Geometry.Size(16,16, 'px'),
        Limit : { min : 8, max : 128 }
    };

    this.cellToPx = function (cells, axe = 'width') {
        return Math.floor(cells * this.Scale.Default[axe]);
    };
    this.pxToCell = function (px, axe = 'width') {
        return Math.round(px / this.Scale.Default[axe]);
    };

    this.convertScaleToDefault = function (px, axe = 'width') {
        return Math.floor(px * this.getDefaultToScaleRatio(axe));
    };
    this.convertDefaultToScale = function (px, axe = 'width') {
        return Math.floor(px * this.getScaleToDefaultRatio(axe));
    };

    this.getDefaultToScaleRatio = function (axe = 'width') {
        return this.Scale.Default[axe] / this.Scale.Current[axe];
    };
    this.getScaleToDefaultRatio = function (axe = 'width') {
        return this.Scale.Current[axe] / this.Scale.Default[axe];
    };
};
