WeberBaseActor = function () {
    this.id = null;
    this.type = '';
    this.state = 0;
    this.Position = new WeberTools.GameLibrary.Geometry.Vector();
    this.Controls = new WeberTools.GameLibrary.Main.Actors.Controls.ActorActionsControlsManager();

    this.ActionsAllowed = {
    };

    this.setId = function (id) {
        this.id = id;
        //propagate
        this.Controls.id = id;
        this.Character.id = id;
    };

    this.initializeBaseActor = function () {
        this.Controls.ActionsAllowed = this.ActionsAllowed;
    };
};