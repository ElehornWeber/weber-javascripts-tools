WeberActorCoreControls = function () {
    this.id = null;
    this.Actions = {};
    this.ActionsAllowed = {};

    this.updateAction = function (actionKey, type) {
        if(!actionKey) { return false; }

        if(!type) { this.removeAction(actionKey); }
        else { this.addAction(actionKey); }
    };

    this.removeAction = function (key) {
        if(this.Actions[key]) {
            this.Actions[key].updateState(0);
            return true;
        }
        return false;
    };
    this.addAction = function (key) {
        if(this.Actions[key]) {
            this.Actions[key].updateState(1);
            return;
        }
        this.Actions[key] = new WeberTools.GameLibrary.Events.ControlAction(key);
    };

    this.getAction = function (key) {
        if(!key || !this.Actions[key]) { return false; }
        return this.Actions[key];
    };

    this.isCurrentAction = function (key) {
        var action = this.getAction(key);
        if(!action) { return false; }
        if(!action.state) { return false; }
        return true;
    };

    this.doAction = function () {
    };

};