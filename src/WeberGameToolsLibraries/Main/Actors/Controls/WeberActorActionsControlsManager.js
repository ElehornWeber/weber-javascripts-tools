WeberActorActionsControlsManager = function () {
    WeberTools.Object.extend(this, new WeberTools.GameLibrary.Main.Actors.Controls.ActorCoreControls());
    this.mode = '';
    this.ActionsType = {};
    this.ActionsBinding = {};
    //0 : not in activity, 1 : down pressed, 2: up event
    this.click = { left: false,  right: false,  middle: false };
    this.keyboard = {};

    this.updateKeyboardEvent = function (actionKey) {};
    this.updateMouseEvent = function (actionKey) {};
    this.mouseWheelEvent = function(Event) {};
    this.mouseMoveEvent = function(Event) {};

    this.updateKeyboard = function (KeyboardEvent) {
        if(this.keyboard[KeyboardEvent.key] && this.keyboard[KeyboardEvent.key].type === KeyboardEvent.type) {
            return false;
        }

        this.keyboard[KeyboardEvent.key] = KeyboardEvent;
        this.updateAction(this.getActionBindingKey(KeyboardEvent.key), KeyboardEvent.type);

        var actionKey = this.getActionBindingKey(KeyboardEvent.key);
        if(!actionKey) { return false; }

        this.updateKeyboardEvent(actionKey);

        var actionType = this.getActionType(actionKey);
        if(!actionType) { return false; }
    };
    this.updateMouse = function (button, type) {
        this.click[button] = type;
        var actionKey = this.getActionBindingKey(button+'Click');
        this.updateAction(actionKey, type);

        var actionType = this.getActionType(actionKey);
        if(!actionType) { return false; }

        this.updateMouseEvent(actionKey);
    };

    this.mouseWheel = function (Event) {
        this.mouseWheelEvent(Event);
    };

    this.mouseMove = function (Event) {
        this.mouseMoveEvent(Event);
    };

    this.getActionType = function(key) {
        if(!this.ActionsType[key])  { return false; }
        return this.ActionsType[key];
    };
    this.getActionBindingKey = function (key) {
        if(this.ActionsBinding[key]) {
            if(typeof this.ActionsBinding[key] === 'object' && this.ActionsBinding[key].hasOwnProperty(this.mode)) {
                return this.ActionsBinding[key][this.mode];
            }
            return this.ActionsBinding[key];
        }
        return false;
    };

    this.keyPress = function (Event) {
        var newKeyboardEvent = new WeberTools.GameLibrary.Events.KeyboardEvent(Event);
        this.updateKeyboard(newKeyboardEvent);
    };

    this.mouseUp = function (Event) {
        var type = 0; var button = '';
        if(Event.button === 0 ) { button = 'left'; }
        else if(Event.button === 2 ) { button = 'right'; }
        this.updateMouse(button, type);
    };
    this.mouseDown = function (Event) {
        var type = 1; var button = '';
        if(Event.button === 0 ) { button = 'left'; }
        else if(Event.button === 2 ) { button = 'right'; }
        this.updateMouse(button, type);
    };

    this.createEvents = function (Origin) {
        var self = this;

        Origin.addEventListener('mousedown', function (Event) { self.mouseDown(Event); });
        Origin.addEventListener('mouseup', function (Event) { self.mouseUp(Event); });
        Origin.addEventListener('mousewheel', function (Event) { self.mouseWheel(Event); });
        Origin.addEventListener('mousemove', function (Event) { self.mouseMove(Event); });
        window.addEventListener('keydown', function (Event) { self.keyPress(Event); });
        window.addEventListener('keyup', function (Event) { self.keyPress(Event); });

    };
};