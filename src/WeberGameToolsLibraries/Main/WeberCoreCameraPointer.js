WeberCoreCameraPointer = function() {
    this.Pointer = new WeberTools.GameLibrary.Geometry.Vector(0,0);

    this.getPointerRealPosition = function () {
        var ratio = this.Scale.Default.width / this.Scale.Current.width;
        return new WeberTools.GameLibrary.Geometry.Vector(
            Math.round(this.Pointer.x * ratio),
            Math.round(this.Pointer.y * ratio)
        );
    };

    this.getPointerMapPosition = function () {
        var RealPosition = this.getPointerRealPosition();
        return new WeberTools.GameLibrary.Geometry.Vector(
            this.Position.x + RealPosition.x,
            this.Position.y + RealPosition.y
        );
    };

    this.getPointerOriginPosition = function () {
        return new WeberTools.GameLibrary.Geometry.Vector(
            this.Pointer.x - this.getHalfCameraSize('width'),
            this.Pointer.y - this.getHalfCameraSize('height')
        );
    };

    this.updatePointerByMouseEvent = function (event) {
        this.Pointer.x = event.x - this.Offset.x;
        this.Pointer.y = event.y - this.Offset.y;
    };
};
