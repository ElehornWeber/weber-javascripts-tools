WeberCamera = function () {
    WeberTools.Object.extend(this, new WeberTools.GameLibrary.Main.CoreCameraScaling());
    WeberTools.Object.implement(this, new WeberTools.GameLibrary.Main.CoreCameraPointer());

    this.Position = new WeberTools.GameLibrary.Geometry.Vector(0,0);
    this.Offset = new WeberTools.GameLibrary.Geometry.Vector(0,0);
    this.Size = new WeberTools.GameLibrary.Geometry.Size(0,0);
    this.CurrentSize = new WeberTools.GameLibrary.Geometry.Size(0,0);
    this.Rectangle = new WeberTools.GameLibrary.Geometry.Rectangle(this.Position, this.CurrentSize);

    this.AttachedActor = null;
    this.ContextSize = new WeberTools.GameLibrary.Geometry.Size(0,0);
    //mode : actor, free, ...
    this.mode = 'actor';

    this.updateScaleEvent = function () {};

    // -1 : stick to origin of the map, 0 : float on the map, 1 : stick to the edge of the map
    this.getCameraPositionState = function(axe) {
        if(!axe || (axe !== 'x' && axe !== 'y')) { return false; }
        if(this.Size[axe] === null) { return 0; }

        var lexic = 'width';
        if(axe === 'y') { lexic = 'height'; }

        var Borders = {
            min: this.AttachedActor.Position[axe] - this.getHalfCameraCurrentSize(lexic) + this.Offset[axe],
            max: this.AttachedActor.Position[axe] + this.getHalfCameraCurrentSize(lexic) - this.Offset[axe]
        };

        var limit = this.ContextSize[lexic];

        if(Borders.min <= 0) { return -1; }
        if(Borders.max >= limit) { return 1; }
        return 0;

    };

    this.getCameraEdge = function (axe = 'x') {
        if(axe === 'x') { return this.Position.x + this.Size.width + this.Offset.x; }
        return this.Position.y + this.Size.height + this.Offset.y;
    };

    this.updateActorPositionMode = function (forced = false) {
        if(!this.AttachedActor) { return false; }

        if(this.AttachedActor && !this.AttachedActor.Position && !forced) { return false; }

        var Position = new WeberTools.GameLibrary.Geometry.Vector(
            this.AttachedActor.Position.x - this.getHalfCameraCurrentSize('width'),
            this.AttachedActor.Position.y - this.getHalfCameraCurrentSize('height')
        );

        Position.x += this.Offset.x;
        Position.y += this.Offset.y;

        var cameraState = {
            x: this.getCameraPositionState('x'),
            y: this.getCameraPositionState('y')
        };

        //Operations if camera is stick to a map border
        if(cameraState.x === 1) { Position.x = this.ContextSize.width - this.getCameraCurrentSize('width'); }
        else if(cameraState.x === -1) { Position.x = 0; }


        if (cameraState.y === 1) { Position.y = this.ContextSize.height - this.getCameraCurrentSize('height'); }
        else if(cameraState.y === -1) { Position.y = 0; }

        //Clean positions
        Position.x = Math.floor(Math.abs(Position.x));
        Position.y = Math.floor(Math.abs(Position.y));

        this.Position.x = Position.x;
        this.Position.y = Position.y;

    };


     this.updateSize = function (newSize) {
        this.Size.width = newSize.width;
        this.Size.height = newSize.height;

        this.updatePosition(true);
    };

     this.updateContextSize = function (Size) {
         this.ContextSize.width = Size.width;
         this.ContextSize.height = Size.height;

         this.updatePosition(true);
     };

    this.updatePosition = function (forced = false) {
        this.CurrentSize.width =  this.convertScaleToDefault(this.Size.width);
        this.CurrentSize.height = this.convertScaleToDefault(this.Size.height);

        if(this.mode === 'actor') {
            this.updateActorPositionMode(forced);
        }

        this.Rectangle.updatePoints();
    };

    this.updateScale = function (scale) {
        if(!scale) {scale = this.Scale.Current.width; }

        if(scale < this.Scale.Limit.min) { scale = this.Scale.Limit.min; }
        else if(scale > this.Scale.Limit.max) { scale = this.Scale.Limit.max; }

        this.Scale.Current.width = scale;
        this.Scale.Current.height = scale;

        this.updateScaleEvent();

        this.updatePosition(true);
    };

    this.getCameraCurrentSize = function (axe = false, ratio = 1) {
        if(!axe) { return this.CurrentSize; }
        return this.CurrentSize[axe] * ratio;
    };
    this.getHalfCameraCurrentSize = function (axe) { return this.getCameraCurrentSize(axe, 0.5); };

    this.setAttachedActor = function (Actor) {
        if(!Actor) { return false; }
        if(!Actor.hasOwnProperty('Position')) { return false; }

        this.AttachedActor = Actor;
        this.updateActorPositionMode();
    };



    //type > 0 : zoom, type < 1 : zoom-out
    this.incrementScale = function (type) {
        var currentScale = this.Scale.Current.width;
        var modifierRatio = 3;
        if(type > 0) {
            currentScale += modifierRatio * type;
            if(currentScale > this.Scale.max) { currentScale = this.Scale.max; }
        } else {
            currentScale += modifierRatio * type;
            if(currentScale < this.Scale.min) { currentScale = this.Scale.min; }
        }

        currentScale = WeberTools.Math.round(currentScale, 2);

        this.updateScale(currentScale);
    };
};