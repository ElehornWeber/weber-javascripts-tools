/**
 * @extends WeberCanvasLayerDrawingLibrary
 * @extends WeberCanvasLayerChunkLibrary
*/
WeberCanvasLayer = function() {
    WeberTools.Object.implement(this, new WeberTools.GameLibrary.Render.CanvasLayerDrawingLibrary());
    WeberTools.Object.implement(this, new WeberTools.GameLibrary.Render.CanvasLayerChunkLibrary());

    this.id = null;
    this.canvas = null;
    this.context = null;
    this.order = 0;
    this.renderMode = 'tilemap';
    this.Position = new WeberTools.GameLibrary.Geometry.Vector();
    this.Size = new WeberTools.GameLibrary.Geometry.Size();
    this.Rectangle = new WeberTools.GameLibrary.Geometry.Rectangle(this.Position, this.Size);
    this.Resolution = new WeberTools.GameLibrary.Geometry.Size();
    this.Scale = {};
    //type is 'dynamic' or 'static'
    this.type = 'dynamic';

    //state : 0 => need to be drawn, 1 => drawn (in case of dynamic, the state will stay at 0.
    this.state = 0;
    this.display = true;

    this.resizeCanvas = function () {
        if(this.Size.width === 0) { return false; }
        if(this.Size.height === 0) { return false; }

        this.canvas.width = this.Size.width;
        this.canvas.height = this.Size.height;

        this.canvas.setAttribute('width', this.Size.width);
        this.canvas.setAttribute('height', this.Size.height);

        this.canvas.style.width = this.Size.width+'px';
        this.canvas.style.height = this.Size.height+'px';

        this.setContextBaseConfig();
    };

    this.updateCanvasSize = function (width, height) {
        this.Size.width = Math.floor(width);
        this.Size.height = Math.floor(height);

        this.Rectangle.updatePoints();
        this.resizeCanvas();
    };

    this.updatePosition = function(x = false, y = false) {
        if( x !== false) { this.Position.x = x; }
        if( y !== false) { this.Position.y = y; }

        this.Rectangle.updatePoints();
    };

    this.updateResolution = function(width, height) {
        this.Resolution.width = width;
        this.Resolution.height = height;
    };

    this.setCanvasBaseConfig = function () {
        this.canvas.style.textRendering = 'geometricPrecision';
        this.canvas.style.imageRendering = 'auto';
    };

    this.setContextBaseConfig = function () {
        this.context.imageSmoothingQuality = 'high';
        this.context.imageSmoothingEnabled = true;

        this.context.save();
    };

    this.clearCanvas = function (x = 0, y = 0, width = 0, height = 0) {
        if(width === 0) { width = this.canvas.width; }
        if(height === 0) { height = this.canvas.height; }

        this.context.clearRect(x, y, width, height);
        this.state = false;
    };

    this.initialize = function () {
        this.canvas = document.createElement('canvas');
        this.context = this.canvas.getContext("2d");

        this.canvas.width = 0;
        this.canvas.height = 0;
        this.canvas.style.width = 0+"px";
        this.canvas.style.height = 0+"px";
    };
    this.initialize();
};
