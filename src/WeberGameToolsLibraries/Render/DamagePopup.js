SimpleAnimation = function() {
    this.start_at = null;
    this.duration = 0;
    this.end_at = null;
    this.endPosition = new Vector();
    this.startPosition = new Vector();
    this.Delta = new Vector();

    this.linearModeProcess = function () {
        var progress = this.getAnimationPercentageProgress();
        return new Vector(
            this.startPosition.x + this.Delta.x * progress,
            this.startPosition.y + this.Delta.y * progress
        );
    };
    this.getAnimationPercentageProgress = function () {
        var percentage = (Date.now() - this.start_at)/this.duration;
        return Tools.Math.round(percentage, 2);
    };
    this.animationEnded = function () { return Date.now() >= this.end_at; };
    this.progress = function () {
        if(this.animationEnded()) { return 'end'; }

        if(this.mode === 'linear') { return this.linearModeProcess(); }
    };

    this.defineDestinationPoint = function (originPosition, deltaPosition, Offset, strictDirection) {
        this.startPosition.x = originPosition.x + Offset.x;
        this.startPosition.y = originPosition.y + Offset.y;

        this.Delta = deltaPosition;

        this.endPosition.x = this.startPosition.x + deltaPosition.x;
        this.endPosition.y = this.startPosition.y + deltaPosition.y;
    };

    this.startAnimation = function () {
        this.start_at = Date.now();
        this.end_at = this.start_at + this.duration;
    };
};

EphemeralAnimation = function (Owner, type) {
    this.Owner = Owner;
    this.type = type;
    this.key = '';
    this.Animation = new SimpleAnimation();
    this.customRender = false;
    this.Position = new Vector();
    this.Offset = new Vector(0, -20);
    this.critical = false;
    this.Text = '';
    this.font = '8px Arial';
    this.color = 'black';
    this.rotation = 0;

    this.endEvent = function () {
        if(!this.Owner) { return false; }
        this.Owner.askToDelete(this.type, this.key);
    };

    this.animationProcess = function () {
        var getProgress = this.Animation.progress();
        if(getProgress === 'end') { this.endEvent(); return; }
        if(getProgress.hasOwnProperty('x') && getProgress.hasOwnProperty('y')) {
            this.Position.x = Math.floor(getProgress.x);
            this.Position.y = Math.floor(getProgress.y);
        }
    };

    this.generateText = function (amount, critical = false, type = 'Health') {
        this.critical = critical;
        this.Text = amount;
        if(type === 'Health') { this.color = 'rgb(230,14,0)'; }
    };

    this.defineStartingPosition = function (x, y) {
        this.Position.x = x;
        this.Position.y = y;
    };

    this.defineAnimationProcess = function (duration, originPosition = new Vector(), deltaPosition = new Vector(), strictDirection = new Vector(false,true), mode = 'linear') {
        this.Animation.duration = duration;
        this.Animation.mode = mode;

        this.Position.x = originPosition.x + this.Offset.x;
        this.Position.y = originPosition.y + this.Offset.y;
        this.Animation.defineDestinationPoint(originPosition, deltaPosition, this.Offset, strictDirection);
    }

};