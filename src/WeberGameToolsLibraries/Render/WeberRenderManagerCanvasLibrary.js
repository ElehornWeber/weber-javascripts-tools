WeberRenderManagerCanvasLibrary = function () {
    this.createMainCanvas = function() {
        if(!this.Main) {
            this.Main = new WeberTools.GameLibrary.Renderer.CanvasLayer();
        }

        this.Main.canvas.setAttribute('id', 'mainCanvas');
        this.Main.updateCanvasSize(this.Resolution.width, this.Resolution.height);

        this.Main.setCanvasBaseConfig();
        this.Main.setContextBaseConfig();

        this.origin.appendChild(this.Main.canvas);
        this.collection.sort(WeberTools.Sort.byOrder);
    };

    /**
     * renderMode = by default it is define on 'tilemap',
     * it will use internal library to render like a map texture. It can also be a function that will render into it.
     */
    this.addCanvasToCollection = function (id, type, renderMode = 'tilemap', order = 0) {
        var newLayer = new WeberTools.GameLibrary.Render.CanvasLayer();

        newLayer.order = order;
        newLayer.renderMode = renderMode;
        newLayer.canvas.style.zIndex = order;
        if(type) { newLayer.type = type; }

        newLayer.id = id;
        newLayer.canvas.setAttribute('id', id+'Canvas');

        newLayer.updateCanvasSize(this.Resolution.width, this.Resolution.height);

        this.collection[newLayer.id] = newLayer;
        this.collection[newLayer.id].setCanvasBaseConfig();
        this.collection[newLayer.id].setContextBaseConfig();

        /*Re order canvas by Order property*/
        var sortedResult =  WeberTools.Sort.sortProperties(this.collection, 'order', true, false);
        this.renderOrder = [];
        for(var i = 0; i< sortedResult.length; i++) { this.renderOrder.push(sortedResult[i][0]); }


        //this.origin.appendChild(newLayer.canvas);
        

        return this.collection[newLayer.id];
    };

    this.getCanvas = function (id) {
        if(!id || !this.collection[id]) { return false; }
        return this.collection[id];
    };

    
    this.resizeMainCanvas = function (width, height) {
        width = Math.floor(width);
        height = Math.floor(height);

        this.Main.updateCanvasSize(width, height);
        this.resizeCanvases('dynamic', width, height);
    };

    this.resizeCanvases = function (type = false, width = 0, height = 0) {
        for(var i in this.collection) {
            var layer = this.collection[i];
            if(type && layer.type !== type) { continue; }

            var canvasSize = new WeberTools.GameLibrary.Geometry.Size(
                this.Resolution.width,
                this.Resolution.height
            );
            if(width && width > 0) { canvasSize.width = width; }
            if(height && height > 0) { canvasSize.height = height; }


            layer.updateCanvasSize(canvasSize.width, canvasSize.height);
        }
    };

    this.updateScreenSize = function (newSize = 'auto') {
        var screenSize = new WeberTools.GameLibrary.Geometry.Size();

        if(newSize === 'auto') {
            var ratio = 16/9;
            screenSize.width = document.documentElement.clientWidth;
            screenSize.height = document.documentElement.clientWidth / ratio;

            if(screenSize.height > document.documentElement.clientHeight) {
                screenSize.height = document.documentElement.clientHeight;
            }

            screenSize.width = Math.floor(screenSize.width);
            screenSize.height = Math.floor(screenSize.height);
        } else if(newSize.hasOwnProperty('width') && newSize.hasOwnProperty('height')) {
            screenSize.width = newSize.width;
            screenSize.height = newSize.height;
        }

        this.resizeMainCanvas(screenSize.width, screenSize.height);
        this.Resolution.width = screenSize.width;
        this.Resolution.height = screenSize.height;

        this.attachedCamera.updateSize(screenSize);

        this.askRenderAll();
    };

    this.createEvents = function () {
        var self = this;
        window.addEventListener('resize', function (Event) {
            self.updateScreenSize();
        });

    };
};