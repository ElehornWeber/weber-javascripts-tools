WeberCanvasLayerChunkLibrary = function() {
    this.Chunks = {
        Size: new WeberTools.GameLibrary.Geometry.Size(),
        Amount: new WeberTools.GameLibrary.Geometry.Size(),
        Ratio: new WeberTools.GameLibrary.Geometry.Size(),
        collection: {}
    };
    this.generateChunkId = function (IndexPosition) { return this.id+'_'+IndexPosition.x+'_'+IndexPosition.y; };

    this.getChunk = function (IndexPosition) {
        var chunkId = this.generateChunkId(IndexPosition);
        if(!this.Chunks.collection.hasOwnProperty(chunkId)) { return false; }
        return this.Chunks.collection[chunkId];
    };

    this.findChunkByPosition = function (Position) {
        var Cell = new  WeberTools.GameLibrary.Geometry.Vector();
        Cell.x = Math.floor(Position.x / this.Chunks.Size.width);
        Cell.y = Math.floor(Position.y / this.Chunks.Size.height);

        return this.getChunk(Cell);
    };

    this.addChunk = function (IndexPosition, ChunkSize = false) {

        if(!ChunkSize) { ChunkSize = this.Chunks.Size; }
        var chunkLayer = new WeberTools.GameLibrary.Render.CanvasLayer();

        chunkLayer.updateCanvasSize(this.Chunks.Size.width, this.Chunks.Size.height);
        chunkLayer.id = this.generateChunkId(IndexPosition);

        chunkLayer.updatePosition(
            this.Chunks.Size.width * IndexPosition.x,
            this.Chunks.Size.height * IndexPosition.y
        );


        this.Chunks.collection[chunkLayer.id] = chunkLayer;

        this.Chunks.Amount.width = Math.max(this.Chunks.Amount.width, IndexPosition.x+1);
        this.Chunks.Amount.height = Math.max(this.Chunks.Amount.height, IndexPosition.y+1);

        return this.Chunks.collection[chunkLayer.id];
    };

    this.generateChunks = function (TotalSize, ChunkAmount = 'auto') {
        if(ChunkAmount === 'auto') {
            ChunkAmount = new WeberTools.GameLibrary.Geometry.Size(TotalSize.width / 1280, TotalSize.height / 720);
        }

        this.updateResolution(this.Size.width, this.Size.height);

        this.Chunks.Size.width = Math.min(TotalSize.width, Math.floor(TotalSize.width / ChunkAmount.width));
        this.Chunks.Size.height = Math.min(TotalSize.height, Math.floor(TotalSize.height / ChunkAmount.height));

        this.Chunks.Ratio.width = this.Size.width / this.Chunks.Size.width;
        this.Chunks.Ratio.height = this.Size.height / this.Chunks.Size.height;

        this.updateCanvasSize(this.Chunks.Size.width, this.Chunks.Size.height);

        for(var y = 0; y < ChunkAmount.height; y++) {
            for(var x = 0; x < ChunkAmount.width; x++) {
                var indexPosition = new WeberTools.GameLibrary.Geometry.Vector(x, y, 'cell');
                var chunkLayer = this.addChunk(indexPosition);
                chunkLayer.updateCanvasSize(this.Chunks.Size.width, this.Chunks.Size.height);


               //document.getElementById('test').appendChild(chunkLayer.canvas);
            }
        }
    };

    this.explodeImageInChunks = function (mapImageLayer, ChunkAmount = 'auto') {
        if(ChunkAmount === 'auto') {
            ChunkAmount = new WeberTools.GameLibrary.Geometry.Size( mapImageLayer.width / 1280, mapImageLayer.height / 720 );
        }

        this.resizeCanvas(mapImageLayer.width, mapImageLayer.height);
        this.Chunks.Size.width = Math.min(mapImageLayer.width, Math.ceil(mapImageLayer.width / ChunkAmount.width));
        this.Chunks.Size.height = Math.min(mapImageLayer.height, Math.ceil(mapImageLayer.height / ChunkAmount.height));

        this.Chunks.Ratio.width = this.Chunks.Size.width / this.Size.width;
        this.Chunks.Ratio.height = this.Chunks.Size.height / this.Size.height;

        for(var y = 0; y < ChunkAmount.height; y++) {
            for(var x = 0; x < ChunkAmount.width; x++) {
                var indexPosition = new WeberTools.GameLibrary.Geometry.Vector(x, y, 'cell');
                var chunkLayer = this.addChunk(indexPosition);

                chunkLayer.drawImage(
                    mapImageLayer,
                    chunkLayer.Position,
                    this.Chunks.Size,
                    new WeberTools.GameLibrary.Geometry.Vector(),
                    this.Chunks.Size
                );

                //document.getElementById('test').appendChild(chunkLayer.canvas);
            }
        }

        this.drawChunks();
        //document.getElementById('test').appendChild(this.canvas);
    };

    this.drawChunk = function (chunkLayer, Camera, Main = false) {
        if(!Camera) { return; }

        if(!Main) { Main = this; }

        Main.context.drawImage(
            chunkLayer.canvas,

            Camera.Position.x - chunkLayer.Position.x,
            Camera.Position.y - chunkLayer.Position.y,
            Camera.convertScaleToDefault(Camera.Size.width),
            Camera.convertScaleToDefault(Camera.Size.height),

            0, 0,
            Camera.Size.width, Camera.Size.height
        );
    };

    this.isHittedChunk = function (chunkLayer, rectangle) {
        if(rectangle.intersectRectangle(chunkLayer.Rectangle)) { return true; }
        return false;
    };
    this.getHittedChunks = function (rectangle) {
        var hitted = [];

        for(var key in this.Chunks.collection) {
            var chunkLayer = this.Chunks.collection[key];

            var result = this.isHittedChunk(chunkLayer, rectangle);

            if(!result) { continue; }

            hitted.push(chunkLayer);
        }
        return hitted;
    };

    this.drawChunks = function (Camera, MainRender) {
        var hittedChunks = this.getHittedChunks(this.Rectangle);

        if(!hittedChunks) { return false; }

        this.clearCanvas();

        for(var i = 0; i < hittedChunks.length; i++) {
            var chunkLayer = hittedChunks[i];
            this.drawChunk(chunkLayer, Camera, MainRender);
        }
    };

    this.drawImageIntoChunk = function (mapImageLayer) {

        var chunkLayers = this.getHittedChunks(mapImageLayer.getRectangle());
        if(!chunkLayers) { return false; }

        for(var i = 0; i < chunkLayers.length; i++) {
            var chunkLayer = chunkLayers[i];
            chunkLayer.context.drawImage(
                mapImageLayer.source,
                mapImageLayer.Position.x - chunkLayer.Position.x,
                mapImageLayer.Position.y - chunkLayer.Position.y,

                mapImageLayer.Size.width, mapImageLayer.Size.height,
            );
            chunkLayer.context.strokeRect(
                0,0,
                mapImageLayer.Size.width, mapImageLayer.Size.height
            );

            chunkLayer.context.fillText(chunkLayer.id,15, 15);

            this.drawChunk(chunkLayer);
        }
    };
};
