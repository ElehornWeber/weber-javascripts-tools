WeberCoreRenderLibrary = function () {
    //WeberTools.Object.extend(this, new EphemeralRenderManager());

    this.canvasAmount = 0;
    this.chunkAmount = 0;

    this.cleanScreen = function () {
        this.Main.context.clearRect(
            0,0,
            this.Main.canvas.width, this.Main.canvas.height
        );

        for(var key in this.collection) {
            var currentCanvasLayer = this.getCanvas(key);
            if(currentCanvasLayer.type !== 'dynamic') { continue; }
            currentCanvasLayer.clearCanvas();
        }
    };

    this.cleanCanvas = function (key) {
        var canvasLayer =  this.getCanvas(key);
        if(!canvasLayer) { return false; }
        canvasLayer.clearCanvas();
    };

    /**@param {WeberCanvasLayer} Canvaslayer*/
    this.chunkCanvasRender = function (Canvaslayer) {
        Canvaslayer.updatePosition(
            this.attachedCamera.Position.x,
            this.attachedCamera.Position.y
        );

        Canvaslayer.drawChunks(this.attachedCamera, this.Main);

        this.defaultCanvasRender(Canvaslayer);
    };

    /**@param {WeberCanvasLayer} Canvaslayer*/
    this.defaultCanvasRender = function (Canvaslayer) {
        if(Canvaslayer.canvas.width === 0 || Canvaslayer.canvas.height === 0) { return false; }

        Canvaslayer.context.save();

        var drawingOrigin = new WeberTools.GameLibrary.Geometry.Vector();
        if(Canvaslayer.type === 'static') {
            drawingOrigin.x = this.attachedCamera.Position.x;
            drawingOrigin.y = this.attachedCamera.Position.y;
        }
        this.Main.context.drawImage(
            Canvaslayer.canvas,

            0,0,
            Canvaslayer.Size.width,
            Canvaslayer.Size.height,

        );

        Canvaslayer.context.restore();
    };
};