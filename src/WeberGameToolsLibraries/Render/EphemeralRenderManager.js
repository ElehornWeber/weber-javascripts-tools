EphemeralRenderManager = function () {
    this.ephemeralCollection = {
        damagesPopup : {},
        pointMarker : {}
    };

    this.askToDelete = function (type, key) {
        if(!this.ephemeralCollection[type]) { return false; }
        var collectionTarget = this.ephemeralCollection[type];

        if(!collectionTarget[key]) { return false; }
        collectionTarget[key] = null;
        delete collectionTarget[key]
    };

    this.getTypeNewId = function (type) {
        if(!this.ephemeralCollection[type]) { return false; }
        var incr = 0;
        while(this.ephemeralCollection[type][type+incr]) { incr++; }
        return type+incr;
    };
    this.addToCollection = function (ephemeralElement) {
        if(!this.ephemeralCollection[ephemeralElement.type]) {
            this.ephemeralCollection[ephemeralElement.type] = {};
        }

        ephemeralElement.key = this.getTypeNewId(ephemeralElement.type);
        if(!ephemeralElement.key) { return false; }

        this.ephemeralCollection[ephemeralElement.type][ephemeralElement.key] = ephemeralElement;
    };

    this.createDamagePopupAnimation = function (CharacterId, type, amount, critical = false) {
        var playerSource = Game.getPlayer(CharacterId);
        if(!playerSource) { return false; }

        var damagePopup = new EphemeralAnimation(this, 'damagesPopup');
        damagePopup.generateText(amount, critical, type);
        damagePopup.defineStartingPosition(playerSource.Character.Position.x, playerSource.Character.Position.y);
        damagePopup.defineAnimationProcess(400, playerSource.Character.Position, new Vector(-12,-20));
        damagePopup.Animation.startAnimation();
        this.addToCollection(damagePopup);
    };

    this.createEphemeralPoint = function (Position, Size, color = 'red') {
        var point = new EphemeralAnimation(this, 'pointMarker');
        point.defineAnimationProcess(5000);
        point.customRender = function (Context, isFirst = false) {
            Context.fillStyle = 'red';
            Context.fillRect(
                Position.x, Position.y,
                Size.width, Size.height
            );
            if(isFirst) {
                Context.moveTo(Position.x, Position.y);
                return;
            }
            Context.lineTo(Position.x, Position.y);
        };
        point.Animation.startAnimation();
        this.addToCollection(point);
    };


    this.renderDamagesPopup = function (canvasId) {
        var canvasLayer = this.getCanvas(canvasId);
        if(!canvasLayer) { return false; }
        for(var popupKey in this.ephemeralCollection.damagesPopup) {
            var popup  = this.ephemeralCollection.damagesPopup[popupKey];
            popup.animationProcess();
            canvasLayer.context.save();
            canvasLayer.context.fillStyle = popup.color;
            canvasLayer.context.fontSize = popup.font;
            canvasLayer.context.fillText(popup.Text, popup.Position.x, popup.Position.y);
            canvasLayer.context.restore();
        }
    };

    this.renderEphemerals = function (canvasId) {
        var canvasLayer = this.getCanvas(canvasId);
        if(!canvasLayer) { return false; }
        var isFirst = true;
        canvasLayer.context.beginPath();
        for(var pointKey in this.ephemeralCollection.pointMarker) {
            var point  = this.ephemeralCollection.pointMarker[pointKey];
            point.animationProcess();
            canvasLayer.context.save();
            if(point.customRender) { point.customRender(canvasLayer.context, isFirst); }
            canvasLayer.context.restore();

            isFirst = false;
        }

        canvasLayer.context.stroke();
    };


};