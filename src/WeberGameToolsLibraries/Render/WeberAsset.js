WeberAsset = function () {
    this.key = null;
    this.state = null;
    this.source = null;
    this.type = null;
    this.rotated = false;

    this.Position = new WeberTools.GameLibrary.Geometry.Vector(0,0);
    this.Size = new WeberTools.GameLibrary.Geometry.Size(0,0);

    this.isSourceReady = function () {
        if(!this.state) { return false; }
        return true;
    };

    this.getSourcePath = function() { return this.type+"/"+this.source; };
    this.setKey = function (filename) { this.key = filename.replace(/\.([A-z]+)/i, ""); };

    this.create = function (key, src, type, onloadedCb = false) {
        var image = new Image();
        image.src = src;
        this.source = image;
        this.type = type;
        this.setKey(key);

        var self = this;
        this.source.onload = function () {
            console.log('image loaded');
            self.state = true;

            if(onloadedCb && typeof onloadedCb === 'function') { onloadedCb(); }
        };
    };
};