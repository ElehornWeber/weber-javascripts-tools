/**
 * @extends RenderManagerCanvasLibrary
 * @implement CoreRenderLibrary
 */
WeberCoreRenderManager = function () {
    WeberTools.Object.implement(this, new WeberTools.GameLibrary.Render.RenderManagerCanvasLibrary());
    WeberTools.Object.implement(this, new WeberTools.GameLibrary.Render.CoreRenderLibrary());

    this.origin = null;
    this.Main = new WeberTools.GameLibrary.Render.CanvasLayer();
    this.attachedCamera = null;

    this.Resolution = new WeberTools.GameLibrary.Geometry.Size(0,0);
    this.lastRender = 0;
    this.renderMaxFps = 40;
    this.globalState = 1;

    this.renderAmount = 0;
    this.renderersAmount = 0;
    this.renderTimeStart = 0;

    this.collection = [];
    this.renderOrder = [];

    this.renderAfterClean = function() {};
    this.renderAfterCanvases = function() {};

    this.pause = function () {
        this.globalState = false;
        this.renderAmount = 0;
        this.renderersAmount = 0;
    };
    this.start = function () {
        this.globalState = true;
        this.renderTimeStart = Date.now();
        this.renderLoop();
    };

    this.getRenderFps = function (type = 'render') {
        var property = this.renderAmount;
        if(type === 'renderers') { property = this.renderersAmount; }
        var timeDelta = Date.now() - this.renderTimeStart;
        return Math.round(property / ( timeDelta / 1000 ));
    };

    this.getRenderersByRender = function () {
        return Math.round(this.renderersAmount / this.renderAmount);
    };

    this.renderLoop = function () {
        var self = this;

        var deltaTime = Date.now() - this.lastRender;
        if (this.renderMaxFps < WeberTools.GameLibrary.Math.msToFps(deltaTime)) {
            return window.requestAnimationFrame(function () {
                self.renderLoop();
            });
        }
        if(!this.globalState) { return false; }

        this.cleanScreen();
        this.renderAfterClean();

        this.renderCanvasCollection();
        this.renderAfterCanvases();

        this.lastRender = Date.now();

        window.requestAnimationFrame(function () { self.renderLoop(); });
    };

    this.renderCanvasCollection = function () {
        for(var i in this.renderOrder) {
            var canvasLayer = this.collection[this.renderOrder[i]];
            canvasLayer.Scale = this.attachedCamera.Scale;
            if(!canvasLayer) { continue; }
            if(canvasLayer.state === 1) { continue; }
            if(!canvasLayer.display) { continue; }


            if(canvasLayer.type === 'chunk') {
                this.chunkCanvasRender(canvasLayer);
            } else {
                this.defaultCanvasRender(canvasLayer);
            }

            this.renderersAmount++;
            //canvasLayer.state = 1;
        }

        this.renderAmount++;
    };

    this.askRender = function (id) {
        var canvasCollection = this.getCanvas(id);
        if(!canvasCollection) { return false; }
        canvasCollection.state = 0;
        return true;
    };

    this.askRenderAll = function () {
        for (var id in this.collection) { this.askRender(id); }
    };

    this.initialize = function (originElement, renderResolution = 'auto') {
        this.origin = originElement;
        if(renderResolution && typeof renderResolution === 'object')  {
            this.Resolution.width = renderResolution.width;
            this.Resolution.height = renderResolution.height;
        }
        this.createMainCanvas();
        this.updateScreenSize(this.Resolution);
    };
};