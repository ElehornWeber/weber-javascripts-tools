WeberMath = function () {
    this.fpsToMs = function (fps) { return 1000/fps; };
    this.msToFps = function (ms) { return Math.round(1/(ms/1000)); };
};