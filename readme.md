#[ OUTDATED, need rewrite ] Weber Js Tools Project

##Ready-To-Use Tools
@Todo

##Autoload for Native Javascript
Aimed to be the closest as possible with the classic autoload in other langages,
WeberTools include this Autoload that can be call super easly.

####First
Sadly you must include the WeberTools manually. Then create a json file, for example `config.json`.
####Second
Fill the `config.json`;
```
[
    { 
        "namespace": "Core","name": "Example",
        instantiate": false
        "dependencies": { 
            "FullNameSpace/Path" : true, 
            "OtherDependency": true  
        } 
    },
    { "namespace": "FullNameSpace", "name": "Path" },
    { "namespace": "", "name": "OtherDependency" }
]
```
- namespace + name = full file path
- In this example, **Core/Example** need **FullNameSpace/Path** and **OtherDependency** to work.
- **instanciate : bool** is used to define if the object will be instanciate in the WeberTools or if it's the object itself
that will be store. **Default: true**

####Third
**Simple Mode**: 
In your head just call `WeberTools.Autoload('config.json')̀`. 

---
As second parameter, use config parameters like 
```
{
    basePath: 'your/src/path' //default : site base root
    scriptLocation: 'your-element-id' //default: document.head
}
```
- Every parameters can be ommited. Just use them if needed.
- Script location must have the good id to include scripts inside.
---

As third parameter, use a callback function . It will be like : 
`WeberTools.Autoload('config.json', {}, function () {
             console.log('The process is ended');
         });`

In the callback, you are 100% SURE that EVERY scripts are loaded. You can then start your app in it. 

---
###Additional infos

To increase the loading speed, there is a  small tips (that work really good !)
- Make the Autoloader work , then, get the object of `WeberTools.CoreAutoload.includeOrder` 
- Then paste it in your **config json** file. It will now load in the faster way !

